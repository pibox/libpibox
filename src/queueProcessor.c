/*******************************************************************************
 * PiBox application library
 *
 * queueProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define QUEUEPROCESSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <pwd.h>

static int queueIsRunning = 0;
static pthread_mutex_t queueProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t queueProcessorThread;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isQueueProcessorRunning
 * Prototype:  int isQueueProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of queueIsRunning variable.
 *========================================================================*/
static int
isQueueProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &queueProcessorMutex );
    status = queueIsRunning;
    pthread_mutex_unlock( &queueProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setQueueProcessorRunning
 * Prototype:  int setQueueProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of queueIsRunning variable.
 *========================================================================*/
static void
setQueueProcessorRunning( int val )
{
    pthread_mutex_lock( &queueProcessorMutex );
    queueIsRunning = val;
    pthread_mutex_unlock( &queueProcessorMutex );
}

/*========================================================================
 * Name:   queueProcessor
 * Prototype:  void queueProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from queue and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * Sleep between empty reads defaults to 100 milliseconds.
 *========================================================================*/
static void *
queueProcessor( void *arg )
{
    PIBOX_MSG_T *msg = NULL;
    PIBOX_REG_T *registration = (PIBOX_REG_T *)arg;

    /* Spin, looking for messages to process. */
    queueIsRunning = 1;
    while( isCLIFlagSet(CLI_SERVER) )
    {
        /* Grab a message. */
        msg = popMsgQueue();
        if ( msg == NULL )
        {
            usleep(50000); // 50 milliseconds
            continue;
        }

        // If a handler is registered, use it.
        // The handler must copy the message if it wants to keep it.
        if ( registration->handler != NULL )
            registration->handler(msg);

        // Free up the message.
        freeMsgNode(msg);
    }

    logger(LOG_INFO, "%s: queueProcessor thread is exiting.\n", PROG);
    queueIsRunning = 0;

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startQueueProcessor
 * Prototype:  void startQueueProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownQueueProcessor().
 *========================================================================*/
void
startQueueProcessor( PIBOX_REG_T *reg )
{
    int rc;

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&queueProcessorThread, NULL, &queueProcessor, (void *)reg);
    if (rc)
    {
        logger(LOG_ERROR, "%s: Failed to create processor thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    logger(LOG_INFO, "%s: Started process thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownQueueProcessor
 * Prototype:  void shutdownQueueProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownQueueProcessor( void )
{
    int timeOut = 1;

    stopCurrent();
    while ( isQueueProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            logger(LOG_ERROR, "%s: Timed out waiting on processor thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(queueProcessorThread);
    logger(LOG_INFO, "%s: queueProcessor has shut down.\n", PROG);
}

