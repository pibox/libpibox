/*******************************************************************************
 * PiBox application library
 *
 * rpi.c:  Functions related to Raspberry Pi config file.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define RPI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <glib.h>

#include "pibox.h"

/*
 * ========================================================================
 * Prototype
 * =======================================================================
 */

static int validate_set(char *name, char *value);
static int validate_hex(char *name, char *value);
static int validate_display_rotate(char *name, char *value);

/*
 * ========================================================================
 * Type definitions and Data structures
 * =======================================================================
 */

// Maximum buffer size used internally
#define MAX_BUF 256

// List of supported config.txt names and their possible values
#define RPI_NAME_MAX 11
static RPI_NAME_T rpi_names[] = {
    {RPI_DISABLE_OVERSCAN,    "0,1",                  validate_set},
    {RPI_OVERSCAN_LEFT,       "int",                  validate_set},
    {RPI_OVERSCAN_RIGHT,      "int",                  validate_set},
    {RPI_OVERSCAN_TOP,        "int",                  validate_set},
    {RPI_OVERSCAN_BOTTOM,     "int",                  validate_set},
    {RPI_HDMI_SAVE,           "0,1",                  validate_set},
    {RPI_HDMI_DRIVE,          "1,2",                  validate_set},
    {RPI_HDMI_MODE,           "0-86",                 validate_set},
    {RPI_DECODE_MPG2,         "hex value",            validate_hex},
    {RPI_DISPLAY_ROTATE,      "0x{012}000{0123}",     validate_display_rotate},
    {RPI_OVERCLOCK,           "700,800,900,950,1000", validate_set}
};

#define RPI_REQUIRED_NAME_MAX 3
static RPI_NAME_T rpi_required_names[] = {
    {RPI_HDMI_IGNORE_EDID_AUDIO,   "1",                         NULL},
    {RPI_DISABLE_SPLASH,           "1",                         NULL},
    {RPI_DTPARAM,                  "act_led_trigger=heartbeat", NULL}
};

// The list of configurations
static GSList *rpi_config = NULL;
static GSList *namelist = NULL;

/*
 *========================================================================
 *========================================================================
 * Validation (private) functions
 * 
 * All validation functions return
 * 1 on success (valid)
 * 0 on failure (not valid)
 *========================================================================
 *========================================================================
 */
static int
validate_set(char *name, char *value)
{
    int num;
    char *ptr;

    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "Name is null in validate_set\n");
        return 0;
    }
    if ( value == NULL )
    {
        piboxLogger(LOG_ERROR, "Value is null in validate_set\n");
        return 0;
    }

    // Make sure it's an integer value
    piboxLogger(LOG_TRACE3, "Testing value %s as integer in validate_set\n", value);
    for ( ptr=value; *ptr != '\0'; ptr++ )
    {
        piboxLogger(LOG_TRACE3, "Testing character %c in validate_set\n", *ptr);
        if ( isdigit(*ptr) == 0 )
        {
            piboxLogger(LOG_ERROR, "Value is not numeric validate_set\n");
            return 0;
        }
    }

    num = atoi(value);
    piboxLogger(LOG_TRACE3, "validate_set: value as num = %d\n", num);

    if ( (strcmp(name, RPI_DISABLE_OVERSCAN)==0) || (strcmp(name, RPI_HDMI_SAVE)==0) )
    {
        switch (num)
        {
            case 0: break;
            case 1: break;
            default: 
                piboxLogger(LOG_ERROR, "Value invalid for %s / %s\n", RPI_DISABLE_OVERSCAN, RPI_HDMI_SAVE);
                return 0;
        }
    }
    else if ( strcmp(name, RPI_HDMI_DRIVE ) == 0 )
    {
        switch (num)
        {
            case 1: break;
            case 2: break;
            default: 
                piboxLogger(LOG_ERROR, "Value invalid for %s\n", RPI_HDMI_DRIVE);
                return 0;
        }
    }
    else if ( strcmp(name, RPI_HDMI_MODE ) == 0 )
    {
        if ( (num<0) || (num>86) )
        {
            piboxLogger(LOG_ERROR, "Value invalid for %s\n", RPI_HDMI_MODE);
            return 0;
        }
    }
    else if ( strcmp(name, RPI_OVERCLOCK ) == 0 )
    {
        switch (num)
        {
            case 700:  break;
            case 800:  break;
            case 900:  break;
            case 950:  break;
            case 1000: break;
            default: 
                piboxLogger(LOG_ERROR, "Value invalid for %s\n", RPI_OVERCLOCK);
                return 0;
        }
    }
    else if ( (strcmp(name, RPI_OVERSCAN_LEFT )   == 0) || 
              (strcmp(name, RPI_OVERSCAN_RIGHT )  == 0) ||
              (strcmp(name, RPI_OVERSCAN_TOP )    == 0) ||
              (strcmp(name, RPI_OVERSCAN_BOTTOM ) == 0) )
    {
        if (num < 0)
        {
            piboxLogger(LOG_ERROR, "Value invalid for overscan. \n");
            return 0;
        }
    }
    return 1;
}

static int
validate_hex(char *name, char *value)
{
    char *ptr;
    if ( value == NULL )
        return 0;

    piboxLogger(LOG_TRACE3, "validate_hex: %s\n", value);
    ptr = value;
    if ( strncmp(value, "0x", 2) == 0 )
    {
        ptr++;
        ptr++;
    }
    piboxLogger(LOG_TRACE3, "validate_hex: pointer = %s\n", ptr);

    // value[] will point to null character if all characters are valid hex
    if (ptr[strspn(ptr, "0123456789abcdefABCDEF")] == 0)
        return 1;
    piboxLogger(LOG_TRACE3, "validate_hex: %c\n", ptr[strspn(ptr, "0123456789abcdefABCDEF")]);
    return 0;
}

static int
validate_display_rotate(char *name, char *value)
{
    char *ptr;

    // Validate it's valid hex, then validate the digits.
    if ( !validate_hex(name, value) )
    {
        piboxLogger(LOG_ERROR, "%s does not have a hex value\n", name);
        return 0;
    }

    ptr = value;
    if ( strncmp(value, "0x", 2) == 0 )
    {
        ptr++;
        ptr++;
    }
    if (strlen(ptr) != 5)
    {
        piboxLogger(LOG_ERROR, "%s value is too short: wanted 5, got %d\n", name, strlen(ptr));
        return 0;
    }

    // Just check the five digits manually
    if ( (*ptr !='0') && (*ptr !='1') && (*ptr !='2') ) 
    {
        piboxLogger(LOG_ERROR, "%s: char 1 of value is invalid; wanted 0,1 or 2 but got %c\n", __FUNCTION__, *ptr);
        return 0;
    }
    ptr++;
    if ( *ptr !='0') 
    {
        piboxLogger(LOG_ERROR, "%s: char 2 of value is invalid; wanted 0 but got %c\n", __FUNCTION__, *ptr);
        return 0;
    }
    ptr++;
    if ( *ptr !='0') 
    {
        piboxLogger(LOG_ERROR, "%s: char 3 of value is invalid; wanted 0 but got %c\n", __FUNCTION__, *ptr);
        return 0;
    }
    ptr++;
    if ( *ptr !='0') 
    {
        piboxLogger(LOG_ERROR, "%s: char 4 of value is invalid; wanted 0 but got %c\n", __FUNCTION__, *ptr);
        return 0;
    }
    ptr++;
    if ( (*ptr !='0') && (*ptr !='1') && (*ptr !='2') && (*ptr !='3') ) 
    {
        piboxLogger(LOG_ERROR, "%s: char 5 of value is invalid; wanted 0-3 but got %c\n", __FUNCTION__, *ptr);
        return 0;
    }
        
    return 1;
}

/*
 *========================================================================
 *========================================================================
 * Static (private) functions, mostly callbacks
 *========================================================================
 *========================================================================
 */

static void
printNames( gpointer item, gpointer user_data )
{
    RPI_NAME_T *entry = (RPI_NAME_T *)item;

    if ( (entry->name==NULL) && (entry->value==NULL) )
        printf("Name/Value is incomplete.\n");
    else if ( entry->value==NULL )
        printf("Name (%s) is missing value. \n", entry->name);
    else if ( entry->name==NULL )
        printf("Name missing for value (%s). \n", entry->value);
    else
        printf("Name/Value: %s / %s\n", entry->name, entry->value);
}

static gint
rpi_config_finder( gconstpointer item, gconstpointer user_data )
{
    RPI_NAME_T *entry = (RPI_NAME_T *)item;
    char *name = (char *)user_data;
    if ( strcmp(entry->name, name) == 0 )
        return 0;
    return 1;
}

static void
rpi_config_save( gpointer item, gpointer user_data )
{
    RPI_NAME_T *entry = (RPI_NAME_T *)item;
    FILE *fd = (FILE *)user_data;

    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "rpi_config_save: fd is NULL.\n");
        return;
    }

    if ( strncmp(entry->name, "#", 1) == 0 )
    {
        fprintf(fd, "%s\n", entry->value);
        return;
    }
    else if ( strncmp(entry->name, "NEWLINE", 7) == 0 )
    {
        fprintf(fd, "\n");
        return;
    }
    else if ( strcmp(entry->name, RPI_DISPLAY_ROTATE ) == 0 )
    {
        // These fields require a hex prefix.
        fprintf(fd, "%s=0x%s\n", entry->name, entry->value);
        return;
    }

    fprintf(fd, "%s=%s\n", entry->name, entry->value);
}

/*
 *========================================================================
 * Name:   isNameSupported
 * Prototype:  void isNameSupported( char *name )
 *
 * Description:
 * Check if the specified name is supported.
 *
 * Returns
 * 1 if the name is supported.
 * 0 if the name is not supported.
 *========================================================================
 */
static int
isNameSupported( char *name )
{
    int i;

    // Iterate over supported names list
    for( i=0; i<RPI_NAME_MAX; i++)
    {
        // test for match - case is important!
        if ( strcmp(rpi_names[i].name, name) == 0 )
            return 1;
    }
    return 0;
}

/*
 *========================================================================
 * Name:   validate
 * Prototype:  void validate( void )
 *
 * Description:
 * Verify required name/value pairs exist in the current configuration.
 * Add missing name/value pairs.
 *
 * Returns
 * 1 if the value is valid for name
 * 0 if the value is not valid for name
 *========================================================================
 */
static int
validate( char *name, char *value )
{
    int i;

    for( i=0; i<RPI_NAME_MAX; i++)
    {
        if ( strcmp(rpi_names[i].name, name) == 0 )
        {
            if ( rpi_names[i].handler != NULL )
                return (*rpi_names[i].handler)(name, value);
            else
                piboxLogger(LOG_ERROR, "Name is missing a handler: %s\n", name);
        }
    }
    piboxLogger(LOG_ERROR, "Invalid name: %s\n", name);
    return 0;
}

/*
 *========================================================================
 *========================================================================
 * Public (API) functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   rpiInit
 * Prototype:  int rpiInit( char *filename )
 *
 * Description:
 * Load and parse the config.txt file into linked list of name/value pairs.
 * The file must exist, even if it's empty.
 *
 * Arguments:
 * char *filename   The name of the configuration file to open.  
 *
 * Returns:
 * 1 on success
 * 0 on failure
 *========================================================================
 */
int
rpiInit( char *filename )
{
    int         i;
    FILE        *fd;
    struct stat stat_buf;
    char        buf[MAX_BUF];
    char        *ptr;
    RPI_NAME_T  *nptr;
    char        *tsave = NULL;
    char        *name  = NULL;
    char        *value = NULL;
    GSList      *entry;

    // Don't allow calling twice.
    if ( rpi_config != NULL )
    {
        piboxLogger(LOG_ERROR, "rpiInit() has already been called.\n");
        return 0;
    }

    // Create the namelist linked list - this is a static list that never changes.
    for(i=0; i<RPI_NAME_MAX; i++)
    {
        nptr = g_malloc(sizeof(RPI_NAME_T));
        nptr->name = g_strdup( rpi_names[i].name );
        nptr->value = g_strdup( rpi_names[i].value );
        nptr->handler = rpi_names[i].handler;
        namelist = g_slist_append(namelist, nptr);
    }

    // Test for existance of file
    if ( stat(filename, &stat_buf) != 0 )
        return 0;

    // Open the config file
    fd = fopen(filename, "r");
    if ( fd == NULL )                                                                            
    {                                                                                                 
        piboxLogger(LOG_ERROR, "Failed to open file: %s - %s\n", filename, strerror(errno));
        return 0;
    }  

    // read each line
    memset(buf, 0, MAX_BUF);
    while( fgets(buf, MAX_BUF-1, fd) != NULL )
    {   
        // Strip leading white space
        ptr = buf;
        piboxStripNewline(ptr);
        ptr = piboxTrim(ptr);

        // Save comments
        if ( buf[0] == '#' )
        {
            nptr = g_malloc(sizeof(RPI_NAME_T));
            nptr->name = g_strdup( "#" );
            nptr->value = g_strdup(buf);
            rpi_config = g_slist_append(rpi_config, nptr);
            continue;
        }

        // Skip blank lines
        if ( strlen(ptr) == 0 )
        {
            nptr = g_malloc(sizeof(RPI_NAME_T));
            nptr->name = g_strdup( "NEWLINE" );
            nptr->value = g_strdup(" ");
            rpi_config = g_slist_append(rpi_config, nptr);
            continue;
        }

        piboxLogger(LOG_TRACE3, "Line: %s\n", buf);

        // Split into name/value pair
        if ( index(ptr, '=') != NULL )
        {
            name = strtok_r(ptr, "=", &tsave);
            if ( name != NULL )
                value = strtok_r(NULL, "=", &tsave);
        }
        else
        {
            name = strtok_r(ptr, " ", &tsave);
            if ( name != NULL )
                value = (char *)(ptr + strlen(name));
        }

        // If no tokens, try the next line.
        if ( (name == NULL) || (value == NULL) )
        {
            piboxLogger(LOG_ERROR, "Name or value is null\n");
            name = NULL;
            value = NULL;
            continue;
        }
        piboxLogger(LOG_TRACE3, "Name/value: %s / %s\n", name, value);

        // validate it; set to default if not validated
        // We only validate those names that are supported.  
        // All others pass through untouched.
        if ( isNameSupported( name ) )
        {
            if ( !validate(name, value) )
            {
                piboxLogger(LOG_ERROR, "Name or value is not valid\n");
                name = NULL;
                value = NULL;
                continue;
            }
        }

        // save in hash table
        piboxLogger(LOG_TRACE3, "Inserting into rpi_config: name=%s, value=%s\n", name, value);
        nptr = g_malloc(sizeof(RPI_NAME_T));
        nptr->name = g_strdup( name );
        nptr->value = g_strdup( g_strdup( value ) );
        rpi_config = g_slist_append(rpi_config, nptr);

        // Reset for next line
        name = NULL;
        value = NULL;
    }

    // close file
    fclose(fd);

    // Verify required fields are included
    for (i=0; i<RPI_REQUIRED_NAME_MAX; i++)
    {
        name = rpi_required_names[i].name;
        entry = g_slist_find_custom(rpi_config, name, (GCompareFunc)rpi_config_finder);
        if ( entry != NULL )
        {
            // Add the missing entry
            value = rpi_required_names[i].value;
            nptr = g_malloc(sizeof(RPI_NAME_T));
            nptr->name = g_strdup( name );
            nptr->value = g_strdup( g_strdup( value ) );
            rpi_config = g_slist_append(rpi_config, nptr);
        }
    }

    return 1;
}

/*
 *========================================================================
 * Name:   rpiSetValue
 * Prototype:  int rpiSet(Value char *name, char *value )
 *
 * Description:
 * Update the specified name with the specified value.
 * 
 * Notes:
 * Validates the name is one of the supported configuration items.
 * See rpiNames().
 *
 * Returns:
 * 0 on success
 * 1 on failure
 *========================================================================
 */
int
rpiSetValue( char *name, char *value )
{
    RPI_NAME_T  *nptr;
    GSList      *entry;

    if ( name == NULL )
    {
        piboxLogger(LOG_ERROR, "rpiSetValue: name is null\n");
        return 1;
    }
    if ( value == NULL )
    {
        piboxLogger(LOG_ERROR, "rpiSetValue: value is null\n");
        return 1;
    }

    piboxLogger(LOG_TRACE3, "Name/value: %s / %s\n", name, value);
    if ( isNameSupported( name ) )
    {
        if ( !validate(name, value) )
        {
            piboxLogger(LOG_ERROR, "Name or value is not valid\n");
            return 1;
        }
        piboxLogger(LOG_TRACE3, "Replacing name in rpi_config\n");
        entry = g_slist_find_custom(rpi_config, name, (GCompareFunc)rpi_config_finder);
        if ( entry != NULL )
        {
            nptr = (RPI_NAME_T *)entry->data;
            if ( nptr->value != NULL )
                g_free(nptr->value);
            nptr->value = g_strdup( value );
        }
        return 0;
    }
    piboxLogger(LOG_ERROR, "Name is not supported\n");
    return 1;
}

/*
 *========================================================================
 * Name:   rpiNames
 * Prototype:  GSList *rpiNames( void )
 *
 * Description:
 * Retrieve a list of supported config.txt names.
 *
 * Returns:
 * A pointer to a GHastTable of strings in the format name:value.
 * The caller must not free the hash table or alter it.
 *========================================================================
 */
GSList *
rpiNames( void )
{
    return namelist;
}

/*
 *========================================================================
 * Name:   rpiGetValue
 * Prototype:  char *rpiGetValue( char *name )
 *
 * Description:
 * Retrieve the current value for the specified name.
 *
 * Returns:
 * A copy of the current value or NULL if the name is not configured or
 * not valid.  Caller must not free pointer returned, if any.
 *========================================================================
 */
char *
rpiGetValue( char *name )
{
    GSList      *entry;
    RPI_NAME_T  *nptr;

    if ( name == NULL )
        return NULL;

    entry = g_slist_find_custom(rpi_config, name, (GCompareFunc)rpi_config_finder);
    if ( entry == NULL )
        return NULL;
    nptr = entry->data;
    return (char *)nptr->value;
}

/*
 *========================================================================
 * Name:   rpiSave
 * Prototype:  int rpiSave( char *filename )
 *
 * Description:
 * Save the current configuration to the specified filename.
 *
 * Arguments:
 * char *filename   The name of the configuration file to open.  
 *
 * Notes:
 * Does not validate the values being saved.  Any validation will have been done
 * by rpiSet().  
 *
 * Returns:
 * 0 on success
 * 1 on failure
 *========================================================================
 */
int
rpiSave( char *filename )
{
    FILE        *fd;

    if ( filename == NULL )
    {
        piboxLogger(LOG_ERROR, "rpiSave: filename is null.\n");
        return 1;
    }

    // Open the config file
    fd = fopen(filename, "w");
    if ( fd == NULL )                                                                            
    {                                                                                                 
        piboxLogger(LOG_ERROR, "Failed to open file: %s - %s\n", filename, strerror(errno));
        return 1;
    }  

    // Iterate over the hash, writing each entry to the file.
    g_slist_foreach( rpi_config, rpi_config_save, fd);

    fclose(fd);
    return 0;
}

/*
 *========================================================================
 * Name:   rpiShow
 * Prototype:  void rpiShow( void )
 *
 * Description:
 * Print the current configuration as name/value pairs.
 *========================================================================
 */
void
rpiShow( void )
{
    // piboxLogger(LOG_TRACE3, "Number of config entries: %d\n", g_hash_table_size(rpi_config));
    g_slist_foreach( rpi_config, printNames, NULL);
}

/*
 *========================================================================
 * Name:   rpiGetConfig
 * Prototype:  GSList *rpiGetConfig( void )
 *
 * Description:
 * Retrieve the current configuration as a GSList.
 *
 * Returns:
 * A pointer to a GSList or NULL if the specified name is not supported.
 *========================================================================
 */
GSList *
rpiGetConfig( void )
{
    return rpi_config;
}
