/*******************************************************************************
 * PiBox application library
 *
 * msgQueue.h:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MSGQUEUE_H
#define MSGQUEUE_H

/*
 * ========================================================================
 * Typedefs
 * =======================================================================
 */
#define Q_IN        1       // The inbound message queue
#define Q_PROC      2       // The processed message queue

/*
 * ========================================================================
 * Globals
 * =======================================================================
 */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef MSGQUEUE_C
extern int         queueSize( int );
extern void        freeMsgNode( PIBOX_MSG_T * );
extern PIBOX_MSG_T *popMsgQueue( void );
extern void        queueMsg( int, int, char *, char * );
extern void        clearMsgQueue( void );
#endif /* MSGQUEUE_C */

#endif /* !MSGQUEUE_H */
