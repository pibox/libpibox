/*******************************************************************************
 * PiBox application library
 *
 * log.h:  Handle log messages
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PIBOXLIBLOG_H
#define PIBOXLIBLOG_H

#include <glib.h>

/*========================================================================
 * Defined values
 *=======================================================================*/

/* Log Levels (actually, verbosity levels) */
#define LOG_MIN             0
#define LOG_ERROR           1
#define LOG_WARN            2
#define LOG_INFO            3
#define LOG_TRACE1          4
#define LOG_TRACE2          5
#define LOG_TRACE3          6
#define LOG_TRACE4          7
#define LOG_TRACE5          8
#define LOG_MAX             LOG_TRACE5+1

// Log Flags
#define LOG_F_TIMESTAMP     0x0001      // Enable timestamps in log messages
#define LOG_F_FIXEDHDR      0x0002      // Use fixed size headers (truncates file/line and drops func)
#define LOG_F_SYSLOG        0x0004      // Enable use of syslog, overriding console output
                                        // but works with file output.

#define piboxLogger(...)      _piboxLogger(__FUNCTION__,__FILE__,__LINE__,__VA_ARGS__)
#define piboxLoggerLocal(...) _piboxLoggerLocal(__FUNCTION__,__FILE__,__LINE__,__VA_ARGS__)

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

// Debug macros - Use these for managing the verbosity of the output. 
#define DBGPrintf(...)  { if (0 < piboxLoggerGetVerbosity()) {fprintf ( stderr, __VA_ARGS__ ); fflush(stderr);} }
#define DBGPrintf2(...) { if (2 <=piboxLoggerGetVerbosity()) {fprintf ( stderr, __VA_ARGS__ ); fflush(stderr);} }
#define DBGPrintf3(...) { if (3 <=piboxLoggerGetVerbosity()) {fprintf ( stderr, __VA_ARGS__ ); fflush(stderr);} }
#define DBGPrintf4(...) { if (4 <=piboxLoggerGetVerbosity()) {fprintf ( stderr, __VA_ARGS__ ); fflush(stderr);} }
#define DBGPrintf5(...) { if (5 <=piboxLoggerGetVerbosity()) {fprintf ( stderr, __VA_ARGS__ ); fflush(stderr);} }
#define DBGPrintf10(...){ if (10<=piboxLoggerGetVerbosity()) {fprintf ( stderr, __VA_ARGS__ ); fflush(stderr);} }

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PIBOXLIBLOG_C
extern void piboxLoggerInit( char * );
extern void piboxLoggerShutdown( void );
extern void _piboxLoggerLocal( const char *func, const char *file, int line, int type, char *format, ... );
extern void _piboxLogger( const char *func, const char *file, int line, int type, char *format, ... );
extern void piboxLoggerVerbosity( gint newVal );
extern gint piboxLoggerGetVerbosity( void );
extern void piboxLoggerSetFlags( guint flags );
extern guint piboxLoggerGetFlags( void );
#endif /* !PIBOXLIBLOG_C */

#endif /* !PIBOXLIBLOG_H */
