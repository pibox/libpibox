/*******************************************************************************
 * PiBox application library
 *
 * msgProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGPROCESSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include "msgProcessor.h"
#include "log.h"

static int piboxLibEnabled = 0;
static int piboxLibIsRunning = 0;
static pthread_mutex_t piboxLibMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t piboxLibThread;
struct ifaddrs *piboxLibIfAddr = NULL;

/*========================================================================
 * Name:   isMsgProcessorRunning
 * Prototype:  int isMsgProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of piboxLibIsRunning variable.
 *========================================================================*/
static int
isMsgProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &piboxLibMutex );
    status = piboxLibIsRunning;
    pthread_mutex_unlock( &piboxLibMutex );
    return status;
}

/*========================================================================
 * Name:   setMsgProcessorRunning
 * Prototype:  int setMsgProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of piboxLibIsRunning variable.
 *========================================================================*/
static void
setMsgProcessorRunning( int val )
{
    pthread_mutex_lock( &piboxLibMutex );
    piboxLibIsRunning = val;
    pthread_mutex_unlock( &piboxLibMutex );
}

/*========================================================================
 * Name:   thisHostMatches
 * Prototype:  int thisHostMatches( char * )
 *
 * Description:
 * Test if the specified IP address matches our own address.
 * 
 * Returns:
 * 0 if the addresses do not match.
 * 1 if the addresses do match.
 *========================================================================*/
static int
thisHostMatches( char *ipaddr )
{
    struct ifaddrs *ifa;
    int family, rc;
    char host[NI_MAXHOST];

    if ( ipaddr == NULL )
        return 0;
    if ( piboxLibIfAddr == NULL )
        return 0;

    for (ifa = piboxLibIfAddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;
 
        family = ifa->ifa_addr->sa_family;
        if (family == AF_INET) 
        {
            rc = getnameinfo(ifa->ifa_addr,
                    (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                          sizeof(struct sockaddr_in6),
                    host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if (rc != 0) 
            {
                logger(LOG_ERROR, "getnameinfo() failed: %s\n", gai_strerror(rc));
                continue;
            }
            if (strcmp(host, ipaddr) == 0 )
                return 1;
        }
    }
    return 0;
}

/*========================================================================
 * Name:   getMyIP
 * Prototype:  void getMyIP( void )
 *
 * Description:
 * Find my IPv4 addresses.
 *========================================================================*/
static void
getMyIP( void )
{
    if (getifaddrs(&piboxLibIfAddr) == -1) 
    {
        logger(LOG_ERROR, "Failed to get my IP addresses.\n");
        return;
    }
}

/*========================================================================
 * Name:   msgProcessor
 * Prototype:  void msgProcessor( CLI_T * )
 *
 * Description:
 * Accept and process (or queue) inbound message.
 *
 * Input Arguments:
 * void *arg    unused
 *
 * Notes:
 * Timeout for a completed read on the inbound channel is set to 100 milliseconds.
 *========================================================================*/
static void *
msgProcessor( void *arg )
{
    int                 sd, insd;
    struct sockaddr_in  cliAddr, servAddr;
    char                ipaddr[INET_ADDRSTRLEN]; 
    socklen_t           cliLen = sizeof(cliAddr);
    int                 header;
    int                 payloadSize;
    char                *payload;
    void                *ptr;
    int                 len;
    int                 bytesLeft;
    int                 timeOut;
    int                 flags;
    int                 msgType;
    int                 msgAction;
    int                 moreFields;
    int                 keepSD;
    int                 on;
    int                 ret;

    /* create socket */
    sd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if(sd<0) {
        perror("cannot open socket ");
        pthread_exit(NULL);
    }
    /* Enable address reuse */
    on = 1;
    ret = setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
 
    /* bind server port */
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(INPORT);
 
    if(bind(sd, (struct sockaddr *) &servAddr, sizeof(servAddr))<0) {
        logger(LOG_ERROR, "%s(%d): cannot bind port \n", __FUNCTION__, __LINE__);
        pthread_exit(NULL);
    }

    /* Set up to listen on the port. */
    listen(sd,5);

    /* Spin, waiting for connections. */
    piboxLibIsRunning = 1;
    while( piboxLibEnabled ) 
    {
        /* Wait for a connection. */
        memset( &cliAddr, 0, cliLen );
        insd = accept(sd, (struct sockaddr *) &cliAddr, &cliLen);
        if(insd<0) 
        {
            if ( errno == EAGAIN )
            {
                /* No message? Wait a bit and try again. */
                usleep(1000);
                continue;
            }
            logger(LOG_ERROR, "%s(%d): Cannot accept connection.\n", __FUNCTION__, __LINE__ );
            pthread_exit(NULL);
        }

        /* Find out who sent the message */
        if ( inet_ntop( AF_INET, &cliAddr.sin_addr, ipaddr, INET_ADDRSTRLEN ) == NULL )
        { 
            logger(LOG_ERROR, "%s(%d): Can't find senders IPV4 address - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }
        logger(LOG_TRACE1, "%s(%d): Accepted server connection from [%s:%d]\n", __FUNCTION__, __LINE__, 
            ipaddr, ntohs( cliAddr.sin_port ));

        /* If it's not from this host, drop the connection. */
        if ( !thisHostMatches(ipaddr) )
        { 
            logger(LOG_ERROR, 
                "%s(%d): Not connected to host that sent data message - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }

        // Set the new socket to non-blocking reads, so we can timeout on them.
        flags = fcntl(insd, F_GETFL, 0);
        fcntl(insd, F_SETFL, flags | O_NONBLOCK);

        /* init header and size */
        header = 0;
        payloadSize = 0;

        /* Read the header. */
        ptr = (void *)&header;
        bytesLeft = 4;
        timeOut = 0;
        while ( (timeOut < 200) && bytesLeft )
        {
            len=read(insd, ptr, bytesLeft);
            if ( len > 0 )
            {
                if ( (bytesLeft-len) >= 0 )
                {
                    ptr += len;
                    bytesLeft -= len;
                }
            }
            usleep(500);
            timeOut ++;
        }
        if (bytesLeft)
        {
            logger(LOG_ERROR, "%s(%d): Failed to get header - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }
        logger(LOG_INFO, "header = %08x\n", header);

        /* Read the size. */
        ptr = (void *)&payloadSize;
        bytesLeft = 4;
        timeOut = 0;
        while ( (timeOut < 200) && bytesLeft )
        {
            len=read(insd, ptr, bytesLeft);
            if ( len > 0 )
            {
                if ( (bytesLeft-len) >= 0 )
                {
                    ptr += len;
                    bytesLeft -= len;
                }
            }
            usleep(500);
            timeOut ++;
        }
        if (bytesLeft)
        {
            logger(LOG_ERROR, "%s(%d): Failed to get size field - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }
        logger(LOG_INFO, "payload size = %08x\n", payloadSize);

        /* Allocate a buffer for the payload */
        payload = NULL;
        if ( payloadSize > 0 )
        {
            payload = (char *)malloc(payloadSize+1);
            if ( payload == NULL )
            {
                logger(LOG_ERROR, "%s(%d): Failed to allocate payload buffer - dropping message.\n",
                    __FUNCTION__, __LINE__ ); 
                close(insd);
                continue;
            }
            memset(payload, 0, payloadSize+1);
    
            /* Grab the payload */
            ptr = (void *)payload;
            bytesLeft = payloadSize;
            timeOut = 0;
            while ( (timeOut < 200) && bytesLeft )
            {
                len=read(insd, ptr, bytesLeft);
                if ( len > 0 )
                {
                    if ( (bytesLeft-len) >= 0 )
                    {
                        ptr += len;
                        bytesLeft -= len;
                    }
                }
                usleep(500);
                timeOut ++;
            }
            if (bytesLeft)
            {
                logger(LOG_ERROR, "%s(%d): Failed to read payload - dropping message.\n", 
                    __FUNCTION__, __LINE__ ); 
                close(insd);
                free(payload);
                continue;
            }
            logger(LOG_INFO, "payload = %s\n", payload);
        }
        else
        {
            logger(LOG_ERROR, "%s(%d): Payload size is zero - dropping message.\n", 
                __FUNCTION__, __LINE__ ); 
            close(insd);
            continue;
        }

        logger(LOG_INFO, "Closing socket\n");
        close(insd);
        insd = -1;

        /* Queue the message. */
        logger(LOG_INFO, "Queueing Inbound Msg\n");
        queueMsg( header, payloadSize, payload );
    }

    close(sd);
    logger(LOG_INFO, "msgProcessor thread is exiting.\n");
    piboxLibIsRunning = 0;
    piboxLibEnabled = 0;

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startMsgProcessor
 * Prototype:  void startMsgProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownMsgProcessor().
 *========================================================================*/
void
startMsgProcessor( void )
{
    int rc;

    // Enable the server
    piboxLibEnabled = 1;

    /* Get my IP address before starting to process inbound message */
    getMyIP();

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&piboxLibThread, NULL, &msgProcessor, (void *)NULL);
    if (rc)
    {
        piboxLibEnabled = 0;
        logger(LOG_ERROR, "Failed to create msgProcessor thread: %s\n", strerror(rc));
        exit(-1);
    }
    logger(LOG_INFO, "Started msgProcessor thread.\n");
    return;
}

/*========================================================================
 * Name:   shutdownMsgProcessor
 * Prototype:  void shutdownMsgProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownMsgProcessor( void )
{
    int timeOut = 1;

    piboxLibEnabled = 0;

    // Free up my IP address list.
    if ( piboxLibIfAddr != NULL )
        freeifaddrs(piboxLibIfAddr);

    while ( isMsgProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            logger(LOG_ERROR, "Timed out waiting on msgProcessor thread to shut down.\n");
            return;
        }
    }
    pthread_detach(piboxLibThread);
    logger(LOG_INFO, "msgProcessor has shut down.\n");
}

