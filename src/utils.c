/*******************************************************************************
 * PiBox application library
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/vt.h>
#include "pibox.h"
#include "piboxPrivate.h"
#include "log.h"
#include "utils.h"

/*
 *========================================================================
 * Name:   piboxTrim
 * Prototype:  char *piboxTrim( char * )
 *
 * Description:
 * Trim whitespace from a buffer by moving the provided pointer to the first
 * position that is not whitespace.
 *
 * Argument:
 * char *ptr    Points to a character buffer to trim.
 *
 * Returns:
 * Pointer to the first non-whitespace character or the NUL terminator.
 * If ptr is NULL, returns NULL.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 * Borrowed from StackOverflow:
 * http://stackoverflow.com/questions/122616/how-do-i-trim-leading-trailing-whitespace-in-a-standard-way
 *========================================================================
 */
char * 
piboxTrim( char *ptr )
{
    if ( ptr == NULL )
        return NULL;

    // Trim leading space
    while(isspace(*ptr)) 
        ptr++;
    return ptr;
}

/*
 *========================================================================
 * Name:   piboxStripNewline
 * Prototype:  void *piboxStripNewline( char * )
 *
 * Description:
 * Strip the newline from the end of the buffer.
 *
 * Notes:
 * Assumes null terminated string as function argument.
 *========================================================================
 */
void
piboxStripNewline( char *buf )
{
    char *ptr = buf;
    if ( buf == NULL )
        return;
    while( (*ptr != '\n') && (*ptr != 0) ) 
        ptr++;
    if( *ptr == '\n' ) 
        *ptr = 0;
}

/*
 *========================================================================
 * Name:   piboxValidIPAddress
 * Prototype:  int piboxValidIPAddress( char * )
 *
 * Description:
 * Validate an IP address. 
 *
 * Returns:
 * 0 if invalid, 1 if valid.
 *========================================================================
 */
int
piboxValidIPAddress( char *ipaddr )
{
    struct sockaddr_in sa;
    if ( ipaddr == NULL )
        return 0;
    int result = inet_pton(AF_INET, ipaddr, &(sa.sin_addr));
    return result;
}

/*
 *========================================================================
 * Name:   piboxToLower
 * Prototype:  void piboxToLower( char * )
 *
 * Description:
 * Convert string to lowercase.  Conversion is done in place.
 *========================================================================
 */
void
piboxToLower( char *str )
{
    char *ptr = str;
    if ( ptr == NULL )
        return;
    for( ; *ptr; ++ptr)
        *ptr = tolower(*ptr);
}

/*========================================================================
 * Name:   piboxTimevalSubtract
 * Prototype:  int piboxTimevalSubtract( struct timeval *result, struct timeval *x, struct timeval *y)
 *
 * Description:
 * Subtract the struct timevals x and y and store the result in
 * the struct timeval result.
 *
 * Returns:
 * 1 if the difference is negative, otherwise 0
 *========================================================================*/
int
piboxTimevalSubtract (struct timeval *result, struct timeval *x, struct timeval *y)
{
    int nsec;
    struct timeval ycopy;

    /* Copy the y value so we don't modify it. */
    memcpy(&ycopy, y, sizeof(struct timeval));

    /* Perform the carry for the later subtraction by updating ycopy. */
    if (x->tv_usec < ycopy.tv_usec) 
    {
        nsec = (ycopy.tv_usec - x->tv_usec) / 1000000 + 1;
        ycopy.tv_usec -= 1000000 * nsec;
        ycopy.tv_sec += nsec;
    }
    if (x->tv_usec - ycopy.tv_usec > 1000000) 
    {
        nsec = (x->tv_usec - ycopy.tv_usec) / 1000000;
        ycopy.tv_usec += 1000000 * nsec;
        ycopy.tv_sec -= nsec;
    }

    /* 
     * Compute the time remaining to wait.  
     * tv_usec is certainly positive.
     */
    result->tv_sec = x->tv_sec - ycopy.tv_sec;
    result->tv_usec = x->tv_usec - ycopy.tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < ycopy.tv_sec;
}

/*========================================================================
 * Name:   piboxGetVTNum
 * Prototype:  int piboxGetVTNum( void )
 *
 * Description:
 * Retrieve the VT number of the current process.  
 *
 * Returns:
 * The number of the VT/tty as a int.
 *========================================================================*/
int
piboxGetVTNum ( void )
{
    int             fd;
    int             vtnum;
    char            buf[7];

    if ((fd = open(VT_ACTIVE_PATH, 0)) == -1) 
    {
        piboxLogger(LOG_ERROR, "Failed to open %s: %s\n", VT_ACTIVE_PATH, strerror(errno));
        return(-1);
    }

    memset(buf, 0, 7);
    if ( read(fd, buf, 7) == -1 )
    {
        piboxLogger(LOG_ERROR, "Failed to read %s: %s\n", VT_ACTIVE_PATH, strerror(errno));
        close(fd);
        return(-1);
    }
    close(fd);
    
    vtnum = atoi((char *)(buf+3));
    piboxLogger(LOG_INFO, "Current VT: %s\n", buf);
    return(vtnum);
}

/*========================================================================
 * Name:   piboxGetDisplaySize
 * Prototype:  void piboxGetDisplaySize( int *x, int *y )
 *
 * Description:
 * Query framebuffer for display size.
 *
 * Returns:
 * The x and y dimenions to the input arguments.
 *========================================================================*/
void
piboxGetDisplaySize ( int *x, int *y )
{
    *x=0;
    *y=0;

    *x = piboxGetDisplayWidth();
    *y = piboxGetDisplayHeight();
}

/*========================================================================
 * Name:   piboxLoadEnv
 * Prototype:  void piboxLoadEnv( void )
 *
 * Description:
 * Load the environment variables file.
 *========================================================================*/
void
piboxLoadEnv( char *src )
{
    char    *tsave     = NULL;
    char    *name      = NULL;
    char    *value     = NULL;
    char    *filename;
    FILE    *fd;
    struct stat     stat_buf;
    char    buf[PATH_MAX];

    if ( src != NULL )
        filename = src;
    else
        filename = ENV_F;

    /* Read in custom environment variables */
    if ( stat(filename, &stat_buf) == 0 )
    {
        piboxLogger(LOG_INFO, "Loading environment updates from %s\n", filename);

        // Open the config file.
        fd = fopen(filename, "r");
        if ( fd == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to open %s: %s\n", filename, strerror(errno));
            return;
        }

        // Parse file:  name=value pairs
        while ( fgets(buf, PATH_MAX, fd) != NULL )
        {
            piboxStripNewline(buf);
            name  = strtok_r(buf, "=", &tsave);
            value = strtok_r(NULL, "=", &tsave);
            if ( (name != NULL) && (value != NULL) )
                setenv(name, value, 1);
        }

        fclose(fd);
    }
    else
        piboxLogger(LOG_WARN, "No environment updates; No such file: %s\n", filename);
}

