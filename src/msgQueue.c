/*******************************************************************************
 * PiBox application library
 *
 * msgQueue.c:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGQUEUE_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include "msgProcessor.h"
#include "msgQueue.h"
#include "log.h"

// The head of the message queues.
static PIBOX_MSG_T *piboxLibInQueue = NULL;    // Inbound messages, yet to be processed.
static PIBOX_MSG_T *piboxLibProcQueue = NULL;  // Processed messages, but not yet expired.
static pthread_mutex_t piboxLibQueueMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *========================================================================
 *========================================================================
 *
 * Queue Management
 *
 *========================================================================
 *========================================================================
 */

/*========================================================================
 * Name:   queueSize
 * Prototype:  int queueSize( int )
 *
 * Description:
 * Retrieve the current size of the queue.
 *
 * Arguments:
 * int queueID      Either Q_IN or Q_PROC
 *
 * Returns:
 * The size of the queue.
 *========================================================================*/
int
queueSize( int queueID )
{
    int             count = 0;
    PIBOX_MSG_T     *ptr  = NULL;

    pthread_mutex_lock( &piboxLibQueueMutex );
    if ( queueID == Q_IN )
        ptr  = piboxLibInQueue;
    else
        ptr  = piboxLibProcQueue;
    while ( ptr!=NULL )
    {
        count++;
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &piboxLibQueueMutex );
    return count;
}

/*========================================================================
 * Name:   freeMsgNode
 * Prototype:  void freeMsgNode( PIBOX_MSG_T * )
 *
 * Description:
 * Free a message queue node.  It should already have been popped from its queue.
 *
 * Input Arguments:
 * PIBOX_MSG_T *node     Node to free.
 *========================================================================*/
void
freeMsgNode( PIBOX_MSG_T *node )
{
    if ( node == NULL )
        return;
    if ( node->payload != NULL )
        free(node->payload);
    free(node);
}

/*========================================================================
 * Name:   popMsgQueue
 * Prototype:  PIBOX_MSG_T *popMsgQueue( void )
 *
 * Description:
 * Pull the next message from the inbound queue, if possible.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.  If found, caller is responsible
 * for freeing the node using freeMsgNode().
 *========================================================================*/
PIBOX_MSG_T *
popMsgQueue( void )
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *next = NULL;

    pthread_mutex_lock( &piboxLibQueueMutex );
    ptr = piboxLibInQueue;
    if ( ptr != NULL )
    {
        logger(LOG_INFO, "Popped a message.\n");
        next = ptr->next;
    }
    piboxLibInQueue = next;
    pthread_mutex_unlock( &piboxLibQueueMutex );
    return ( ptr );
}

/*========================================================================
 * Name:   queueMsg
 * Prototype:  void queueMsg( int, int, char * )
 *
 * Description:
 * Queue a message for processing.
 * The new message goes on the end of the list.
 *
 * Input Arguments:
 * int header            Header for the message, which defines action, type, etc.
 * int size              Size of the payload.
 * char *payload         Payload pulled from message.
 *========================================================================*/
void 
queueMsg( int header, int size, char *payload)
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *last = NULL;

    pthread_mutex_lock( &piboxLibQueueMutex );
    ptr = piboxLibInQueue;
    while (ptr != NULL)
    {
        last = ptr;
        ptr = ptr->next;
    }
    ptr = (PIBOX_MSG_T *)malloc(sizeof(PIBOX_MSG_T));
    ptr->next = NULL;
    ptr->header = header;
    ptr->size = size;
    ptr->payload = payload;

    if ( last == NULL )
        piboxLibInQueue = ptr;
    else
        last->next = ptr;

    pthread_mutex_unlock( &piboxLibQueueMutex );
    logger(LOG_INFO, "Inbound queue size: %d\n", queueSize(Q_IN));
}

/*========================================================================
 * Name:   clearQueue
 * Prototype:  clearQueue( void )
 *
 * Description:
 * Clear the inbound message queue.
 *========================================================================*/
void
clearMsgQueue( void )
{
    PIBOX_MSG_T *msg = NULL;
    while ( (msg=popMsgQueue()) != NULL )
        freeMsgNode(msg);
}

