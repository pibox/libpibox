/*******************************************************************************
 * PiBox application library
 *
 * utils.h:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PIBLOXLIBUTILS_H
#define PIBLOXLIBUTILS_H

#include <sys/time.h>

/*========================================================================
 * Defined values
 *=======================================================================*/
#define GET_VT_CMD "ps axo tty,fname | grep '%s' | grep -v grep | tr -s ' '| cut -f1 -d' '"
#define VT_ACTIVE_PATH "/sys/class/tty/tty0/active"

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PIBLOXLIBUTILS_C
extern char *piboxTrim( char *ptr );
extern int   piboxValidIPAddress( char *ipaddr );
extern void  piboxToLower( char *str );
extern void  piboxStripNewline( char *buf );
extern int   piboxTimevalSubtract (struct timeval *result, struct timeval *x, struct timeval *y);
extern int   piboxGetVTNum ( void );
extern void  piboxGetDisplaySize ( int *x, int *y );
extern void  piboxLoadEnv( char *src );
#endif /* !PIBLOXLIBUTILS_C */

#endif /* !PIBLOXLIBUTILS_H */
