/*******************************************************************************
 * PiBox application manager
 *
 * touchProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef TOUCHPROCESSOR_H
#define TOUCHPROCESSOR_H

#include <pthread.h>

/* Where the input devices are (for touchscreens, if we're using one). */
#define INPUT_DEV_DIR   "/dev/input"
#define F_DISPLAYCFG    "/etc/pibox-config"
#define F_TS_EDID       "/etc/ts/edid"
#define F_TS_DEVICES    "/etc/ts/ts_device"
#define LCD_S           "LCD"

/* Callback types */
#define TOUCH_REGION    1
#define TOUCH_ABS       2

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

typedef struct _REGION_T {
    int x;
    int y;
} REGION_T;

/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef TOUCHPROCESSOR_C
#endif /* !TOUCHPROCESSOR_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef TOUCHPROCESSOR_C

extern void piboxTouchSignalHandler( void );
extern void piboxTouchStartProcessor( void );
extern void piboxTouchShutdownProcessor( void );
extern void piboxTouchRegisterCB( void *cb, int );
extern char *piboxTouchGetDeviceName( void );

#endif /* !TOUCHPROCESSOR_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !TOUCHPROCESSOR_H */
