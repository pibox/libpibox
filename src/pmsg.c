/*******************************************************************************
 * PiBox application library
 *
 * message.c:  Communications functions for talking between components
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 *
 * This does not support the full Message Format as described on the wiki:
 *   http://www.graphics-muse.org/wiki/pmwiki.php/RaspberryPi/MessageFlow
 * This only supports app->pibox messaging and return data.
 ******************************************************************************/
#define PMSG_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <glib.h>
#include "pibox.h"

/*
 *========================================================================
 * Private functions
 *========================================================================
 */

/*
 *========================================================================
 * Name:   validTypeAndAction
 * Prototype:  gint validTypeAndAction( guint8 type, guint8 action )
 *
 * Description:
 * Tests to see if the specified type and associated action are valid
 * for app->piboxd messaging.
 *
 * Argument:
 * guint8   type            One of the following:
 * guint8   action          One of the following:
 *
 * Returns:
 * 0 if the type and associated action are invalid.
 * 1 if the type and associated action are valid.
 *========================================================================
 */
static gint
validTypeAndAction( guint8 type, guint8 action )
{
    switch (type)
    {
        /* Stream a webcam */
        case MT_STREAM:
            switch(action)
            {
                case MA_START:
                case MA_END:
                case MA_START_LOW:
                case MA_START_MED:
                case MA_START_FULL:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Managed a heartbeat - this is a keep-alive request. */
        case MT_HEARTBEAT:
            switch(action)
            {
                case MA_NONE:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Retrieve or set IoT device info */
        case MT_DEVICE:
            switch(action)
            {
                case MA_GETALL:
                case MA_GET:
                case MA_SET:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Network set/get requests. */
        case MT_NET:
            switch(action)
            {
                case MA_GETIFLIST:
                case MA_GETIF:
                case MA_GETDNS:
                case MA_GETNETTYPE:
                case MA_GETAP:
                case MA_GETWIRELESS:
                case MA_SETIPV4:
                case MA_SETAP:
                case MA_SETWIRELESS:
                case MA_SETNETTYPE:
                case MA_GETMAC:
                case MA_GETIP:
                case MA_MFLIST:
                case MA_RESTART:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Set/get PiBox PW info. (not related to system users) */
        case MT_PW:
            switch(action)
            {
                case MA_SAVE:
                case MA_GET:
                case MA_DEL:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Start an application. */
        case MT_START:
            switch(action)
            {
                case MA_NONE:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Stop an application. */
        case MT_STOP:
            switch(action)
            {
                case MA_NONE:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Keypress event from remote controller to be passed through to managed application. */
        case MT_KEY:
            switch(action)
            {
                case 0:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* System requests, like "reboot" */
        case MT_SYS:
            switch(action)
            {
                case MA_REBOOT:
                    return 1;
                case MA_KEYB:
                    return 1;
                case MA_ROTATE:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Ironman pairing requests. */
        case MT_IRONMAN:
            switch(action)
            {
                case MA_PAIR_IOT:
                case MA_PAIR_JARVIS:
                    return 1;
                default:
                    return 0;
            }
            break;

        default:
            return 0;
    }
}

/*
 *========================================================================
 * Name:   requiresPayload
 * Prototype:  gint requiresPayload( guint8 type, guint8 action, guint8 subaction )
 *
 * Description:
 * Tests to see if the specified type and action require a payload.
 *
 * Argument:
 * guint8   type            See validTypeAndAction()
 * guint8   action          See validTypeAndAction()
 *
 * Returns:
 * 1 if a payload is required.
 * 0 if a payload is not required.
 *========================================================================
 */
static gint
requiresPayload( guint8 type, guint8 action, guint8 subaction )
{
    switch (type)
    {
        case MT_STREAM:
            switch(action)
            {
                case MA_START:
                case MA_END:
                case MA_START_LOW:
                case MA_START_MED:
                case MA_START_FULL:
                    return 1;
                default:
                    return 0;
            }
            break;

        case MT_DEVICE:
            switch(action)
            {
                case MA_GET:
                case MA_SET:
                    return 1;

                case MA_GETALL:
                default:
                    return 0;
            }
            break;

        case MT_NET:
            switch(action)
            {
                case MA_SETIPV4:
                case MA_SETAP:
                case MA_SETWIRELESS:
                case MA_SETNETTYPE:
                case MA_GETMAC:
                case MA_GETIP:
                case MA_MFLIST:
                    return 1;

                case MA_GETIFLIST:
                case MA_GETIF:
                case MA_GETDNS:
                case MA_GETNETTYPE:
                case MA_GETAP:
                case MA_GETWIRELESS:
                case MA_RESTART:
                default:
                    return 0;
            }
            break;

        case MT_PW:
            switch(action)
            {
                case MA_SAVE:
                case MA_DEL:
                    return 1;

                case MA_GET:
                default:
                    return 0;
            }
            break;

        case MT_KEY:
            return 1;

        case MT_SYS:
            return 0;

        /* These aren't used - the REST API handles pairing. */
        case MT_IRONMAN:
            return 0;

        default:
            return 0;
    }
}

/*
 *========================================================================
 * Name:   requiresReturn
 * Prototype:  gint requiresReturn( guint8 type, guint8 action )
 *
 * Description:
 * Tests to see if the specified type is valid.
 *
 * Argument:
 * guint8   type            See validType()
 * guint8   action          See validAction()
 *
 * Returns:
 * 0 if the return field is not required.
 * 1 if the return field is required.
 *========================================================================
 */
static gint
requiresReturn( guint8 type, guint8 action )
{
    switch (type)
    {
        case MT_DEVICE:
            switch(action)
            {
                case MA_GET:
                case MA_GETALL:
                    return 1;
                case MA_SET:
                default:
                    return 0;
            }
            break;

        /* Network set/get requests. */
        case MT_NET:
            switch(action)
            {
                case MA_GETIFLIST:
                case MA_GETIF:
                case MA_GETDNS:
                case MA_GETNETTYPE:
                case MA_GETAP:
                case MA_GETWIRELESS:
                case MA_GETMAC:
                case MA_GETIP:
                    return 1;
                default:
                    return 0;
            }
            break;

        /* Set/get PiBox PW info. (not related to system users) */
        case MT_PW:
            switch(action)
            {
                case MA_GET:
                    return 1;
                default:
                    return 0;
            }
            break;

        default:
            return 0;
    }
}

/*
 *========================================================================
 * Name:   readFromServer
 * Prototype:  gint readFromServer( void *ptr, gint length, gint sock )
 *
 * Description:
 * Read data from a non-blocking socket into the specified buffer.
 *
 * Argument:
 * void     *ptr            The buffer to fill
 * gint     length          The number of bytes to read from the socket.
 * gint     sock            The socket to read from
 *
 * Returns:
 * 0 if the read is unsuccessful
 * 1 if the read is successful
 *========================================================================
 */
static gint
readFromServer( void *ptr, gint length, gint sock )
{
    gint bytesLeft;
    gint timeOut;
    gint len;

    piboxLogger(LOG_INFO, "Data: %08x\n", ptr);

    // Get the length of the return data
    bytesLeft = length;
    timeOut = 0;
    while ( (timeOut < 200) && bytesLeft )
    {   
        len=read(sock, ptr, bytesLeft);
        if ( len > 0 )
        {   
            if ( (bytesLeft-len) >= 0 )
            {   
                ptr += len;
                bytesLeft -= len;
            }
        }
        usleep(5000);
        timeOut ++;
    }
    if (bytesLeft)
        return 0;
    return 1;
}

/*
 *========================================================================
 * Public API
 *========================================================================
 */

/*
 *========================================================================
 * Name:   piboxMsg
 * Prototype:  gint piboxMsg( guint8 type, guint8 action, guint8 subaction, char *tag, guint payloadSize, gchar *payload, guint *returnSize, gchar **returnData )
 *
 *
 * Description:
 * Send a message using the PiBox Message Flow protocol.  Connects to a piboxd daemon on localhost only.
 *
 * Argument:
 * guint8   type            A message type.  See pmsg.h.
 * guint8   action          An action for the specified type.  See pmsg.h.
 * guint8   subaction       A subaction of the specified action.  See pmsg.h.
 * gchar    *tag            A 36 character UUID in string format.  May be NULL if the type, action
 *                          and subaction do not require it.
 * gint     payloadSize     Length of byte array, if the message requires one.
 * gchar    *payload        Pointer to byte array, if the message requires one.
 * guint    *returnSize     Optional: The size of the receive buffer provided, if any.  Ignored if returnData is NULL.
 * gchar    **returnData    Pointer buffer where return data will be placed, if required.
 *
 * Returns:
 * 0 on success
 * 1 on failure
 * If message returns data then the a buffer pointer will be returned in the returnData argument.
 * If the server fails to return data but the type/action/subaction requires it then this 
 * function returns an error.
 *
 * Notes:
 * For definitions of types actions and subactions see 
 * http://www.graphics-muse.org/wiki/pmwiki.php/RaspberryPi/MessageFlow
 *
 * Caller is reponsible for freeing the returned buffer, if any.
 *
 * Types/actions/subactions supported are listed in validTypeAndAction().
 *========================================================================
 */
gint
piboxMsg( 
        guint8 type, 
        guint8 action, 
        guint8 subaction, 
        gchar  *tag, 
        gint   payloadSize, 
        gchar  *payload, 
        gint   *returnSize, 
        char   **returnData 
        )
{
    guint       header;
    gint        sock;
    guint       getReply = 0;
    guint       sendPayload = 0;
    struct sockaddr_in serverAddr;
    gint        msgLength = payloadSize;
    gint        returnLength = 0;
    gchar       *data = NULL;

    if ( !validTypeAndAction(type, action) )
    {
        piboxLogger(LOG_ERROR, "Invalid type and/or action specified: %d, %d\n", type, action);
        return 1;
    }

    sendPayload = requiresPayload(type,action,subaction);
    if ( sendPayload && (payload == NULL) )
    {
        piboxLogger(LOG_ERROR, "Missing required payload\n");
        return 1;
    }

    // Do we need a reply?
    getReply = requiresReturn(type,action);
    if ( getReply && (returnData == NULL) )
    {
        piboxLogger(LOG_ERROR, "returnData pointer is NULL but type returns data.\n");
        return 1;
    }

    // Create the socket for sending the message.
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        piboxLogger(LOG_ERROR, "Failed to get a socket: %s\n", strerror(errno));
        return 1;
    }

    // Configure the socket.
    memset(&serverAddr, 0, sizeof(serverAddr));           /* Zero out structure */
    serverAddr.sin_family      = AF_INET;                 /* Internet address family */
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");  /* Server is on localhost */
    serverAddr.sin_port        = htons(PIBOXD_PORT);      /* Server port */

    /* Connect to the server */
    if (connect(sock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
    {
        piboxLogger(LOG_ERROR, "Failed to get a socket: %s\n", strerror(errno));
        return 1;
    }

    // Build the header
    header = (subaction <<16) | (action << 8) | type;

    /* Send the header */
    if (send(sock, (char*)&header, sizeof(guint), 0) != sizeof(guint))
    {
        piboxLogger(LOG_ERROR, "Failed to send header: %s\n", strerror(errno));
        close(sock);
        return 1;
    }

    /* Send the payloadSize */
    piboxLogger(LOG_INFO, "Sending the payloadSize: %d\n", msgLength);
    if (send(sock, (char*)&msgLength, sizeof(guint), 0) != sizeof(guint))
    {
        piboxLogger(LOG_ERROR, "Failed to send msgLength: %s\n", strerror(errno));
        close(sock);
        return 1;
    }

    /*
     * Send the tag if it is present.
     * The tag is always 36 characters long.
     */
    if ( tag != NULL )
    {
        piboxLogger(LOG_INFO, "Sending tag: %d\n", msgLength);
        if (send(sock, (char*)tag, 36, 0) != 36)
        {
            piboxLogger(LOG_ERROR, "Failed writing tag: %s", strerror(errno));
            return 1;
        }
    }
    else
        piboxLogger(LOG_INFO, "Skipping tag\n");

    /* Send payload, if required. */
    if (sendPayload)
    {
        piboxLogger(LOG_INFO, "Sending payload: %s\n", payload);
        if (send(sock, (char*)payload, payloadSize, 0) != payloadSize)
        {
            piboxLogger(LOG_ERROR, "Failed to send payload: %s\n", strerror(errno));
            close(sock);
            return 1;
        }
    }
    else
        piboxLogger(LOG_INFO, "Skipping payload\n");

    // If required, get the reply and pass it to the caller.
    if ( getReply )
    {
        piboxLogger(LOG_INFO, "Retrieving response\n");

        // Make it non-blocking
        fcntl(sock, F_SETFL, O_NONBLOCK);

        // Get the length of the return data
        piboxLogger(LOG_INFO, "Retrieving response length\n");
        if ( !readFromServer((void *)&returnLength, 4, sock) )
        {
            piboxLogger(LOG_ERROR, "%s(%d): Failed to get response length.\n");
            close(sock);
            return 1;
        }

        // Create a buffer to put the data into.
        piboxLogger(LOG_INFO, "Read size from server: %d\n", returnLength);
        data = (gchar *)malloc(returnLength);

        if ( !readFromServer((void *)data, returnLength, sock) )
        {
            piboxLogger(LOG_ERROR, "Failed to receive return data: %s\n", strerror(errno));
            close(sock);
            return 1;
        }

        if ( data == NULL )
            piboxLogger(LOG_ERROR, "Returned data is NULL\n");
        else
            piboxLogger(LOG_INFO, "Data: %s \n", data);

        // Save the buffer and size to the caller.
        *returnData = data;
        if ( returnSize != NULL )
            *returnSize = returnLength;
    }
    else
    {
        /* Caller better not have anything pointed to by returnData! */
        piboxLogger(LOG_INFO, "Skipping response\n");
    }

    return 0;
}
