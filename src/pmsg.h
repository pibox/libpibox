/*******************************************************************************
 * PiBox application library
 *
 * piboxd.h:  Communications functions for talking to piboxd daemon.
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PIBOXD_H
#define PIBOXD_H

/*========================================================================
 * Type definitions
 *=======================================================================*/

#define PIBOXD_PORT     13910

/*========================================================================
 * Defined values
 *=======================================================================*/
// Message types (byte 1 of PIBOX_MSG_T header)
#define MT_STREAM           1   // Webcam stream
#define MT_HEARTBEAT        2   // Application/device heartbeat
#define MT_NET              3   // Network management
#define MT_PW               4   // User management
#define MT_DEVICE           5   // DEPRECATED: REST API handles IoT device queries
#define MT_START            6   // Application start
#define MT_STOP             7   // Application stop
#define MT_KEY              8   // Remote control keypress
#define MT_SYS              9   // TBD
#define MT_IRONMAN          10  // TBD

// No action
#define MA_NONE             0   // Used when no action is associated with a type.

// Message actions (byte 2 of PIBOX_MSG_T header): MT_STREAM
#define MA_START            1
#define MA_END              2
#define MA_START_LOW        3
#define MA_START_MED        4
#define MA_START_FULL       5

// Message actions (byte 2 of PIBOX_MSG_T header): MT_NET
#define MA_GETIFLIST        1   // Retrieve list of interfaces
#define MA_GETIF            2   // Get specified IF configuration (IPv4, gateway, etc.)
#define MA_GETDNS           3   // Retrieve DNS configuration
#define MA_GETNETTYPE       4   // Are we a wireless client or an access point?
#define MA_GETAP            5   // Get Access Point SSID, channel, password
#define MA_GETWIRELESS      6   // Get Wireless SSID, security type and password
#define MA_SETIPV4          7   // Set IPV4 configuration (IPv4, nameserver, gateway, etc.)
#define MA_SETAP            8   // Set Access Point SSID, channel, password
#define MA_SETWIRELESS      9   // Set Wireless SSID, security type and password
#define MA_SETNETTYPE       10  // Set network type: wireless client or access point
#define MA_GETMAC           11  // Retrieve MAC address for specified interface
#define MA_GETIP            12  // Retrieve IP address for specified interface
#define MA_MFLIST           13  // Write MAC address filter list
#define MA_RESTART          14  // Restart network

// MAC Filter (MA_MFLIST) sub types
#define MS_GET              1   // Get mac filter list (hostapd.accept)
#define MS_SET              2   // Set mac filter list (hostapd.accept)

// Message actions (byte 2 of PIBOX_MSG_T header): MT_PW
#define MA_SAVE     1
#define MA_GET      2
#define MA_DEL      3

// Message actions (byte 2 of PIBOX_MSG_T header): MT_DEVICE
#define MA_GETALL   1
#define MA_GET      2
#define MA_SET      3

// Message actions (byte 2 of PIBOX_MSG_T header): MT_SYS
#define MA_REBOOT   1
#define MA_KEYB     2           // Keyboard (or input device) plug event.
#define MA_ROTATE   3           // Rotate display.

// Message actions (byte 2 of PIBOX_MSG_T header): MT_IRONMAN
// These aren't used - the REST API handles pairing.
#define MA_PAIR_IOT     1
#define MA_PAIR_JARVIS  2

/*========================================================================
 * Prototypes
 *=======================================================================*/

#ifndef PMSG_C
extern gint piboxMsg( guint8, guint8, guint8, gchar *, gint, gchar *, gint *, char **);
#endif /* !PMSG_C */

#endif /* !PIBOXD_H */
