/*******************************************************************************
 * PiBox application library
 *
 * piboxPrivate.h:  Library private header 
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PIBOXLIBPRIVATE_H
#define PIBOXLIBPRIVATE_H

/*========================================================================
 * Defined values
 *=======================================================================*/

#define MAXBUF              4096
#define PROC_CPU            "/proc/cpuinfo"
#define REVISION_S          "Revision"
#define ENV_F               "/etc/pibox-env"

/* Raspberry Pi Revision IDs */
#define PIBOX_RPIZERO        0x9        /* RPi Zero/Zero W/Zero 2 W has three different model ids */
#define PIBOX_RPIZEROW       0xC
#define PIBOX_RPIZERO2W      0x12

#define PIBOX_RPI1BPLUS1_S  "0010"      /* RPi B+ has three different model ids, two old style, 1 new */
#define PIBOX_RPI1BPLUS2_S  "0013"
#define PIBOX_RPI1BPLUS3     0x3

#define PIBOX_RPI2B          0x4        /* RPi 2B+ is the only Model 2 supported. */

#define PIBOX_RPI3BPLUS      0xd        /* RPi 3B+. */
#define PIBOX_RPI3B          0x8        /* RPi 3B. */

#define PIBOX_RPI4B          0x11       /* RPi 4B. */

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

/*========================================================================
 * Prototypes
 *=======================================================================*/

/*========================================================================
 * Include other library headers
 *=======================================================================*/

#endif /* !PIBOXLIBPRIVATE_H */
