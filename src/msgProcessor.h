/*******************************************************************************
 * PiBox application library
 *
 * msgProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MSGPROCESSOR_H
#define MSGPROCESSOR_H

#include <pthread.h>

#define INPORT                  13912

/*========================================================================
 * Type definitions
 *=======================================================================*/
typedef struct _pibox_msg {
    struct _pibox_msg   *next;
    int                 header;         // 4 byte header: app defined
    int                 size;           // size of payload
    char                *payload;       // payload (path and filename)
} PIBOX_MSG_T;

typedef struct _pibox_registration {
    int     flags;                      // See PIBOX_FL_ flags 
    void    (*handler)(PIBOX_MSG_T *);  // Callback for inbound messages.
} PIBOX_REG_T;

#define PIBOX_FL_DISABLE_QPROC  0x0001  // Don't use the queueProcessor

/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef MSGPROCESSOR_C

extern pthread_mutex_t queueMutex;

#endif /* !MSGPROCESSOR_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef MSGPROCESSOR_C

extern void startMsgProcessor( void );
extern void shutdownMsgProcessor( void );

#endif /* !MSGPROCESSOR_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !MSGPROCESSOR_H */
