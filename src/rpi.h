/*******************************************************************************
 * PiBox application library
 *
 * rpi.h:  Functions related to Raspberry Pi config file.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef RPI_H
#define RPI_H

#include <glib.h>

/*========================================================================
 * Defined values
 *=======================================================================*/
#define RPI_DISABLE_OVERSCAN        "disable_overscan"
#define RPI_OVERSCAN_LEFT           "overscan_left"
#define RPI_OVERSCAN_RIGHT          "overscan_right"
#define RPI_OVERSCAN_TOP            "overscan_top"
#define RPI_OVERSCAN_BOTTOM         "overscan_bottom"
#define RPI_HDMI_SAVE               "hdmi_safe"
#define RPI_HDMI_DRIVE              "hdmi_drive"
#define RPI_HDMI_MODE               "hdmi_mode"
#define RPI_DECODE_MPG2             "decode_MPG2"
#define RPI_DISPLAY_ROTATE          "display_rotate"
#define RPI_OVERCLOCK               "overclock"
#define RPI_GPU_256                 "gpu_mem_256"
#define RPI_GPU_512                 "gpu_mem_512"
#define RPI_HDMI_IGNORE_EDID_AUDIO  "hdmi_ignore_edid_audio"
#define RPI_DISABLE_SPLASH          "disable_splash"
#define RPI_DTPARAM                 "dtparam"

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/
// Files have a path and a filename.
typedef struct _rpi_name {
    char    *name;                     // config.txt name
    char    *value;                    // Value(s) for this name
    int     (*handler)(char *, char*); // Valiation handler
} RPI_NAME_T;

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef RPI_C
extern int         rpiInit( char *filename );
extern int         rpiSetValue( char *name, char *value );
extern GSList     *rpiNames( void );
extern char       *rpiGetValue( char *name );
extern int         rpiSave( char *name );
extern void        rpiShow( void );
extern GSList     *rpiGetConfig( void );
#endif /* !RPI_C */

#endif /* !RPI_H */
