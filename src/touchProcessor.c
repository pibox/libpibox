/*******************************************************************************
 * PiBox application manager
 *
 * touchProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TOUCHPROCESSOR_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <linux/input.h>
#include <tslib.h>

#include "pibox.h"

static int serverIsRunning = 0;
static pthread_mutex_t touchProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t touchProcessorThread;

/* Add supported touchscreens to this list. */
static const char *ts_name_default[] = {
        "FT5406 memory based driver",
        "ILITEK ILITEK-TP",
        "raspberrypi-ts",
        "ADS7846 Touchscreen",
        NULL
};
static char *ts_name = NULL;

static REGION_T region[3];
static char ts_server = 0;
static char *active_device_name = NULL;

/* Callback when touch in a region is identified. */
void (*touchRegionCB)(int) = NULL;
void (*touchABSCB)(void *) = NULL;

/*
 * ========================================================================
 * STATIC FUNCTIONS
 * ========================================================================
 */

/*========================================================================
 * Name:   setupTouchRegions
 * Prototype:  void setupTouchRegions( void )
 *
 * Description:
 * Sets the bounds for touch regions.  These regions define actions that
 * the app will take but those actions are app specific.
 *
 * The region bounds are based on the resolution of the display.
 *========================================================================*/
static void
setupTouchRegions( void )
{
    int     width;
    int     height;
    int     region_w;
    int     region_h;
    int     i;

    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width == 0) || (height==0) )
    {
        piboxLogger(LOG_INFO, "Display resolution has not been configured.\n");
    }
    piboxLogger(LOG_INFO, "Width / Height: %d / %d\n", width, height);

    /* Split into 3 equal parts each */
    region_w = (int)(width / 3);
    region_h = (int)(height / 3);

    /* Save the region starting points */
    region[0].x = 0;
    region[1].x = region_w;
    region[2].x = region_w*2;
    
    region[0].y = 0;
    region[1].y = region_h;
    region[2].y = region_h*2;

    for(i=0;i<3;i++)
        piboxLogger(LOG_INFO, "region x.y: %d / %d\n", region[i].x, region[i].y);
}

/*========================================================================
 * Name:   ts_setup
 * Prototype:  struct tsdev *ts_setup( const char *dev_name, int nonblock )
 *
 * Description:
 * Implementation of ts_setup from newer versions of tslib because the one
 * in the currently used version of Buildroot doesn't have it.
 *
 * Returns:
 * A pointer to an initialized tsdev structure.
 *========================================================================*/
struct tsdev *ts_setup(const char *dev_name, int nonblock)
{
    char            name[256] = "Unknown";
    char            buf[512];
    const char      *defname = NULL;
    struct tsdev    *ts = NULL;
    int             i = 0;
    int             fd;
    DIR             *pDir;
    struct dirent   *pDirent;

    /* Reset the active device name. */
    if ( active_device_name != NULL )
    {
        free(active_device_name);
        active_device_name = NULL;
    }

    /* We can set the device name in the config file */
    piboxLogger(LOG_INFO, "TSLIB_TSDEVICE: %s\n", getenv("TSLIB_TSDEVICE"));
    dev_name = dev_name ? dev_name : getenv("TSLIB_TSDEVICE");
    piboxLogger(LOG_INFO, "dev_name: %s\n", dev_name);

    if (dev_name != NULL)
    {
        ts = ts_open(dev_name, nonblock);
        fd = ts_fd(ts);
        ioctl(fd, EVIOCGNAME (sizeof (name)), name);
    } 
    else 
    {
        /* Not specified, so search for a matching device */
        pDir = opendir(INPUT_DEV_DIR);
        if (pDir != NULL )
        {
            while ((pDirent = readdir(pDir)) != NULL) 
            {
                if ( (strcmp(pDirent->d_name, ".") == 0) ||
                     (strcmp(pDirent->d_name, "..") == 0) )
                {
                    continue;
                }
                sprintf(buf, "%s/%s", INPUT_DEV_DIR, pDirent->d_name);
                piboxLogger(LOG_INFO, "Reading %s\n", buf);
                ts = ts_open(buf, nonblock);
                if ( ts == NULL )
                {
                    piboxLogger(LOG_INFO, "Failed to open %s\n", buf);
                    continue;
                }
                fd = ts_fd(ts);
                ioctl(fd, EVIOCGNAME (sizeof (name)), name);

                /* Compare against externally configured touchscreen, if any. */
                if ( ts_name != NULL )
                {
                    piboxLogger(LOG_INFO, "ts_name %s vs event name %s\n", ts_name, name);
                    if (strcasecmp(ts_name, name ) == 0 )
                    {
                        piboxLogger(LOG_INFO, "Using device %s\n", buf);
                        defname = ts_name;
                    }
                    else
                        piboxLogger(LOG_INFO, "No match.\n");
                }

                /* If no external configuration, check the defaults. */
                else if ( defname == NULL )
                {
                    i=0;
                    defname = ts_name_default[i++];
                    while (defname != NULL)
                    {
                        piboxLogger(LOG_INFO, "Have %s, want %s\n", defname, name);
                        if (strcasecmp(defname, name ) == 0 )
                        {
                            piboxLogger(LOG_INFO, "Using device %s\n", buf);
                            break;
                        }
                        else
                            defname = ts_name_default[i++];
                    }
                }
                
                if ( defname != NULL )
                {
                    piboxLogger(LOG_INFO, "defname = %s\n", defname);
                    dev_name = defname;
                    break;
                }
                else
                {
                    piboxLogger(LOG_INFO, "Getting next input device.\n");
                    ts_close(ts);
                    ts = NULL;
                }
            }
            closedir (pDir);
        }
        else
            return ts;
    }

    /* if detected try to configure it */
    if ( dev_name == NULL ) 
    {
        piboxLogger(LOG_ERROR, "TSLIB: Failed to find a known touchscreen device.\n");
        return NULL;
    }

    /* if detected try to configure it */
    if (ts && ts_config(ts) != 0) 
    {
        piboxLogger(LOG_ERROR, "TSLIB: ts_config failed.\n");
        ts_close(ts);
        return NULL;
    }
    piboxLogger (LOG_INFO, "Reading From : %s (%s)\n", dev_name, name);

    /* Set the active device name */
    active_device_name = strdup(dev_name);

    return ts;
}

/*========================================================================
 * Name:   isTouchProcessorRunning
 * Prototype:  int isTouchProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of serverIsRunning variable.
 *========================================================================*/
static int
isTouchProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &touchProcessorMutex );
    status = serverIsRunning;
    pthread_mutex_unlock( &touchProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setTouchProcessorRunning
 * Prototype:  int setTouchProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of serverIsRunning variable.
 *========================================================================*/
static void
setTouchProcessorRunning( int val )
{
    pthread_mutex_lock( &touchProcessorMutex );
    serverIsRunning = val;
    pthread_mutex_unlock( &touchProcessorMutex );
}

/*========================================================================
 * Name:   touchProcessor
 * Prototype:  void touchProcessor( CLI_T * )
 *
 * Description:
 * Read touch event and forward it to any listeners.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * Timeout for a completed read on the inbound channel is set to 100 milliseconds.
 *
 * Blocks on the read of the device.  Use SIGINT to break out of this.
 *========================================================================*/
static void *
touchProcessor( void *arg )
{
    int                 i,j;
    int                 found;
    int                 ret;
    int                 local_ts_server=0;
    char                pressed = 0;
    struct tsdev        *ts;
    struct ts_sample    samp;
    struct ts_sample    last_samp;
    struct timeval      diff;
    REGION_T            pt;

    /* Init */
    memset( &samp, 0, sizeof(struct ts_sample));
    memset( &last_samp, 0, sizeof(struct ts_sample));

    /* Spin, waiting for connections. */
    setTouchProcessorRunning(1);

    ts = ts_setup(NULL, 1);
    if (!ts)
    {
        piboxLogger(LOG_ERROR, "TSLIB: ts_setup failed.\n");
        exit(1);
    }

    /* Mark the loop as enabled. */
    pthread_mutex_lock( &touchProcessorMutex );
    ts_server = 1;
    pthread_mutex_unlock( &touchProcessorMutex );

    /* We need to use a mutex here, so spin forever but break if we change states. */
    while( 1 )
    {
        /*
         * Get and test loop enable variable.  Break from loop if we've been disabled.
         */
        pthread_mutex_lock( &touchProcessorMutex );
        local_ts_server = ts_server;
        pthread_mutex_unlock( &touchProcessorMutex );

        if ( !local_ts_server )
            break;

        /* Now block on the read of the device.  Use shutdownTouchProcessor() to break out of this. */
        ret = ts_read(ts, &samp, 1);

        /* Non-blocking will return 0 (no reads) or <0 on error. */
        if (ret == 0)
        {
            /* Slow down the non-blocking reads so ts doesn't monopolize the CPU */
            usleep(250000);
            continue;
        }
        else
        {
            /*
            * A SIGINT will cause this, in which case we are exiting the loop.
            * But we don't do anything as external calls will determine whether we exit or not.
            */
            if (ret < 0)
            {
                piboxLogger(LOG_WARN, "TSLIB: ts_read failed, possibly due to SIGINT.\n");
                continue;
            }
        }

        /* We only wanted 1 event - make sure that's all we got. */
        if (ret != 1)
        {
            piboxLogger(LOG_ERROR, "TSLIB: ts_read() returned %d.\n", ret);
            continue;
        }

        /* 
         * Catch only the press, not the release.
         * This just skips every other event since we always get events in pairs (press/release).
         */
        pressed = 1 - pressed;
        if ( !pressed )
        {
            piboxLogger(LOG_INFO, "TSLIB: not pressed.\n");
            continue;
        }

        piboxLogger(LOG_INFO, "TSLIB: sample %ld.%06ld: %6d %6d %6d\n", 
                samp.tv.tv_sec, samp.tv.tv_usec, samp.x, samp.y, samp.pressure);
        piboxLogger(LOG_INFO, "TSLIB: last   %ld.%06ld: %6d %6d %6d\n", 
                last_samp.tv.tv_sec, last_samp.tv.tv_usec, last_samp.x, last_samp.y, last_samp.pressure);

        /*
         * Check for repeat presses, like when multiple touches are recorded with a single press.
         * They might also be release events.  These would happen in very short time frames (<250ms).
         * Note that the debounce filter should be setup to prevent this.
         */
        if ( (last_samp.tv.tv_sec != 0) && (last_samp.tv.tv_usec != 0) )
        {
            if ( piboxTimevalSubtract (&diff, &samp.tv, &last_samp.tv) == 0 )
            {
                piboxLogger(LOG_INFO, "1. diff.sec: %d  -- diff.usec: %d\n", diff.tv_sec, diff.tv_usec);
                if ( (diff.tv_sec == 0) && (diff.tv_usec < 250000) )
                {
                    piboxLogger(LOG_INFO, "--- Skipping fast repeat\n");
                    continue;
                }
            }
            else
                piboxLogger(LOG_INFO, "2. diff.sec: %d  -- diff.usec: %d\n", diff.tv_sec, diff.tv_usec);
        }

        /* Save sample */
        memcpy(&last_samp, &samp, sizeof(struct ts_sample));

        /* Send absolute coordinates if requested. */
        if ( touchABSCB != NULL )
        {
            piboxLogger(LOG_INFO, "Calling registered touch callback with ABS.\n");
            pt.x = samp.x;
            pt.y = samp.y;
            (*touchABSCB)(&pt);
        }
        else
        {
            piboxLogger(LOG_INFO, "ABS callback is NULL\n");
        }

        found=0;
        for(i=2;i>=0;i--)
        {
            if ( region[i].x <= samp.x )
            {
                for(j=2;j>=0;j--)
                {
                    if ( region[j].y <= samp.y )
                    {
                        piboxLogger(LOG_INFO, "--- Region %d %d\n", i, j);
                        found=j*3+i;
                        break;
                    }
                }
                if (found)
                    break;
            }
        }
        
        if ( !found )
            piboxLogger(LOG_INFO, "--- No Region found\n");
        else
        {
            /* Call the registered callback, if any. */
            if ( touchRegionCB != NULL )
            {
                piboxLogger(LOG_INFO, "Calling registered touch callback with region: %d\n", found);
                (*touchRegionCB)(found);
            }
            else
            {
                piboxLogger(LOG_INFO, "Region callback is NULL\n");
            }
        }
    }

    /* Close the device now that we're done with it. */
    ts_close(ts);

    piboxLogger(LOG_INFO, "touchProcessor thread is exiting.\n");
    setTouchProcessorRunning(0);

    /* Call return() instead of pthread_exit so valgrid doesn't report dlopen problems. */
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   piboxTouchInit
 * Prototype:  void piboxTouchInit( void )
 *
 * Description:
 * Initializes the touch subsystem.
 * Currently that's just configuring for custom touch device names.
 *========================================================================*/
static void
piboxTouchInit( void )
{
    struct stat stat_buf;
    FILE        *fd;
    char        edid[32];
    char        *line=NULL;
    size_t      len=0;
    char        *ptr, *edidLine, *deviceLine;
    char        *tsave;

    /* Check if edid file exists - would be generated by firstboot script. */
    if ( stat(F_TS_EDID, &stat_buf) == 0 )
    {
        piboxLogger(LOG_INFO, "Found EDID data: %s\n", F_TS_EDID);

        /* Read it. */
        fd = fopen(F_TS_EDID, "r");
        if (fd == NULL )
        {
            piboxLogger(LOG_ERROR, "Can't open EDID data: %s: %s\n", F_TS_EDID, strerror(errno));
            return;
        }
        memset(edid, 0, 32);
        ptr = fgets(edid, 31, fd);
        fclose(fd);
        if ( ptr == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to read EDID data: %s\n", F_TS_EDID);
            return;
        }
        piboxStripNewline(edid);
        piboxLogger(LOG_INFO, "Configured EDID: %s\n", edid);

        /* Open ts_devices */
        fd = fopen(F_TS_DEVICES, "r");
        if (fd == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to read devices configuration: %s\n", F_TS_DEVICES);
            return;
        }

        /* Read each line */
        while( getline(&line, &len, fd) != -1 )
        {
            /* Skip comments in file */
            if ( *line == '#' )
                continue;

            /* Parse device name */
            piboxStripNewline(line);
            edidLine = strtok_r(line, ":", &tsave);
            if ( ptr == NULL )
                continue;
            deviceLine = strtok_r(NULL, ":", &tsave);
            if ( ptr == NULL )
                continue;
            piboxLogger(LOG_INFO, "Devices entry: EDID: %s, DEVICE: %s\n", edidLine, deviceLine);

            /* Check for a match against the configured EDID */
            if ( strcasecmp(edid, edidLine) == 0 )
            {
                /* Match found - set ts_name */
                piboxLogger(LOG_INFO, "Found matching device: %s\n", edidLine);
                ts_name = strdup(deviceLine);
                fclose(fd);
                return;
            }
        }
        free(line);
        fclose(fd);
    }
}

/*
 * ========================================================================
 * Public Functions
 * ========================================================================
 */

/*========================================================================
 * Name:   piboxTouchStartProcessor
 * Prototype:  void piboxTouchStartProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownTouchProcessor().
 *========================================================================*/
void
piboxTouchStartProcessor( void )
{
    int rc;

    /* Prevent double initialization. */
    if ( isTouchProcessorRunning() )
    {
        piboxLogger(LOG_ERROR, "Touchprocessor is already running.\n");
        return;
    }

    /* Setup the touch regions. */
    piboxLoadDisplayConfig();
    setupTouchRegions();

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&touchProcessorThread, NULL, &touchProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "Failed to create touchProcessor thread: %s\n", strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "Started touchProcessor thread.\n");
    return;
}

/*========================================================================
 * Name:   piboxTouchSignalHandler
 * Prototype:  void piboxTouchSignalHandler( void )
 *
 * Description:
 * Disable touch thread, typically via a signal handler which needs to be fast.
 *========================================================================*/
void
piboxTouchSignalHandler( void )
{
    pthread_mutex_lock( &touchProcessorMutex );
    ts_server = 0;
    pthread_mutex_unlock( &touchProcessorMutex );
}

/*========================================================================
 * Name:   piboxTouchShutdownProcessor
 * Prototype:  void piboxTouchShutdownProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
piboxTouchShutdownProcessor( void )
{
    int timeOut = 1;

    piboxTouchSignalHandler();
    while ( isTouchProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "Timed out waiting on touchProcessor thread to shut down.\n");
            return;
        }
    }
    pthread_detach(touchProcessorThread);
    piboxLogger(LOG_INFO, "touchProcessor has shut down.\n");
}

/*========================================================================
 * Name:   piboxTouchRegisterCB
 * Prototype:  void piboxTouchRegisterCB( void (*cb)(), int type )
 *
 * Arguments:
 * void (*cb)()     Function pointer that will be called with touch data.
 * int  type        Either TOUCH_ABS or TOUCH_REGION
 *                  TOUCH_ABS will pass a pointer to a xxx type.
 *                  TOUCH_REGION will pass an int from 0-8.
 *
 * Description:
 * Shut down message processing thread.
 *
 * Notes:
 * The callbacks have two types.  For TOUCH_ABS the callback must take an
 * int argument.  For TOUCH_REGION the callback takes a pointer to a xxx
 * structure.  The callback must NOT free that pointer.
 *========================================================================*/
void
piboxTouchRegisterCB( void (*cb)(), int type )
{
    /* Initialize the touch subsystem. */
    piboxTouchInit();

    if ( type == TOUCH_REGION )
    {
        touchRegionCB = cb;
        piboxLogger(LOG_INFO, "Registered a touch region callback.\n");
        return;
    }
    else if ( type == TOUCH_ABS )
    {
        touchABSCB = cb;
        piboxLogger(LOG_INFO, "Registered a touch ABS callback.\n");
        return;
    }

    piboxLogger(LOG_ERROR, "Invalid callback type: %d\n", type);
    return;
}

/*========================================================================
 * Name:   piboxTouchGetDeviceName
 * Prototype:  char *piboxTouchGetDeviceName( void )
 *
 * Description:
 * Retrieve the name of the currently active device, or NULL if no device
 * is active.
 *
 * Returns:
 * Pointer to string with name or NULL.  Caller must free returned pointer.
 *
 * Notes:
 * This can be called before the wrapper library has been started or after.
 *========================================================================*/
char *
piboxTouchGetDeviceName( )
{
    struct tsdev    *ts = NULL;
    char            *copy = NULL;

    if ( ! isTouchProcessorRunning() )
    {
        /* Initialize the wrapper library. */
        ts = ts_setup(NULL, 1);
        if (!ts) 
        {
            piboxLogger(LOG_ERROR, "ts_setup failed.\n");
            return(NULL);
        }
    }

    if ( active_device_name != NULL )
    {
        copy = strdup( active_device_name );
    }
    else
        piboxLogger(LOG_INFO, "active_device_name is NULL\n");

    /* Don't leave the system initialized if it wasn't previously. */
    if ( ! isTouchProcessorRunning() )
    {
        ts_close(ts);
    }
    return( copy );
}
