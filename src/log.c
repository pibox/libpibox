/*******************************************************************************
 * PiBox application library
 *
 * log.c:  Handle log messages
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PIBOXLIBLOG_C

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <glib.h>
#include <syslog.h>

/* We don't need syslog's LOG_INFO and it conflicts with ours. */
#ifdef LOG_INFO
#undef LOG_INFO
#endif

#include "log.h"
#include "pibox.h"
#include "piboxPrivate.h"

// File descriptor for log file
static FILE *logFD = NULL;
static char *logFile = NULL;
static gint verbose = -1;
static guint log_flags = 0;

static char *LogType[9] = {
    "UNKNOWN",
    "ERROR",
    "WARN",
    "INFO",
    "TRACE1",
    "TRACE2",
    "TRACE3",
    "TRACE4",
    "TRACE5"
};

static pthread_mutex_t piboxLogMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 *========================================================================
 *========================================================================
 * Public API functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   piboxLoggerInit
 * Prototype:  void piboxLoggerInit( char *dest )
 *
 * Description:
 * Initialize the log file, if necessary. 
 *
 * Arguments:
 * dest    Output file to use, if any. 
 *========================================================================
 */
void
piboxLoggerInit( char *dest )
{
    verbose = -1;

    if ( logFD != NULL )
        return;

    if ( dest != NULL )
    {
        logFile = (char *)malloc(strlen(dest)+1);
        sprintf(logFile, "%s", dest);
        unlink(logFile);
        logFD = fopen (logFile, "a");
    }

    if ( log_flags & LOG_F_SYSLOG )
        openlog(NULL, LOG_NDELAY, LOG_USER);
}

/*
 *========================================================================
 * Name:   piboxLoggerShutdown
 * Prototype:  void piboxLoggerShutdown( void )
 *
 * Description:
 * Closes the log file, if necessary. 
 *========================================================================
 */
void
piboxLoggerShutdown( void )
{
    if ( logFD == NULL )
        return;

    fclose(logFD);
    logFD = NULL;
    free(logFile);

    if ( log_flags & LOG_F_SYSLOG )
        closelog();
}

/*
 *========================================================================
 * Name:   piboxLoggerVerbosity
 * Prototype:  void piboxLoggerVerbosity( gint )
 *
 * Description:
 * Set the verbosity level for the logger.
 *========================================================================
 */
void
piboxLoggerVerbosity( gint newVal )
{
    verbose = newVal;
}

/*
 *========================================================================
 * Name:   piboxLoggerSetFlags
 * Prototype:  gint piboxLoggerSetFlags( guint flags )
 *
 * Description:
 * Set the logging system flags.
 *
 * Available flags options:
 * LOG_F_TIMESTAMP      Enable use of timestamps in log messages.
 * LOG_F_FIXEDHDR       Use fixed size headers (truncates file/line and drops func)
 * LOG_F_SYSLOG         Enable use of syslog, overriding console output
 *                      but works with file output.
 *========================================================================
 */
void
piboxLoggerSetFlags( guint flags )
{
    log_flags = flags;
}

/*
 *========================================================================
 * Name:   piboxLoggerGetFlags
 * Prototype:  guint piboxLoggerGetFlags( unsigned int flags )
 *
 * Description:
 * Get the verbosity level for the logger.
 *========================================================================
 */
guint
piboxLoggerGetFlags( void )
{
    return log_flags;
}

/*
 *========================================================================
 * Name:   piboxLoggerGetVerbosity
 * Prototype:  gint piboxLoggerGetVerbosity( void )
 *
 * Description:
 * Get the verbosity level for the logger.
 *========================================================================
 */
gint
piboxLoggerGetVerbosity( void )
{
    return verbose;
}

/*
 *========================================================================
 * Name:   piboxLoggerLocal
 * Prototype:  void piboxLoggerLocal( void )
 *
 * Description:
 * Log a message locally, to screen or file. 
 *========================================================================
 */
void
_piboxLoggerLocal( const char *func, const char *file, int line, int type, const char *fmt, ... )
{
    va_list         ap;
    static char     buf[MAXBUF];
    static char     hdr[MAXBUF];
    char            *ptr;
    char            *msg;
    time_t          now;

    pthread_mutex_lock( &piboxLogMutex );

    memset(buf, 0, MAXBUF);
    va_start (ap, fmt);
    vsnprintf(buf, MAXBUF-1, fmt, ap);
    va_end (ap);

    /* Clamp the type, to avoid errors. */
    if ( (type <= LOG_MIN) && (type >= LOG_MAX) )
        type = 1;

    // Build a line header
    memset(hdr, 0, MAXBUF);
    ptr = hdr;
    if ( log_flags & LOG_F_TIMESTAMP )
    {
        now = time(NULL);
        sprintf(hdr, "%08x ", (uint32_t) now);
        ptr = (char *)(hdr + strlen(hdr));
    }
    if ( log_flags & LOG_F_FIXEDHDR )
    {
        sprintf(ptr, "[%13s:%04d]", file, line);
    }
    else
    {
        sprintf(ptr, "%s[%s:%d]", func, file, line);
    }

    /* ---- SYSLOG OUTPUT ---- */
    /* This sometimes drops messages - not sure why. */
    if ( log_flags & LOG_F_SYSLOG )
    {   
        if ( verbose >= type )
        {
            msg = (char *)calloc(1, strlen(hdr) + strlen(LogType[type]) + strlen(buf) + 4);
            sprintf(msg, "%s %s %s", hdr, LogType[type], buf);
            piboxStripNewline(msg);
            syslog(LOG_USER, "%s", msg);
            free(msg);
        }
    }

    /* ---- FILE OUTPUT ---- */
    if ( logFile != NULL )
    {
        if ( verbose >= type )
        {
            /* If outputFilename has been set, open it for appending. */
            if ( logFD == NULL )
            {
                pthread_mutex_unlock( &piboxLogMutex );
                return;
            }
            fprintf(logFD, "%s %s %s", hdr, LogType[type], buf);
            fflush(logFD);
        }

        // Don't log to console if logging to a file.
        pthread_mutex_unlock( &piboxLogMutex );
        return;
    }

    /* ---- CONSOLE OUTPUT (if not logging to syslog) ---- */
    if ( (type > LOG_MIN) && (type < LOG_MAX) && (!(log_flags & LOG_F_SYSLOG)) )
    {
        if ( verbose >= type )
            DBGPrintf ("%s %s %s", hdr, LogType[type], buf);
    }

    pthread_mutex_unlock( &piboxLogMutex );
}

/*
 *========================================================================
 * Name:   piboxLogger
 * Prototype:  void piboxLogger( void )
 *
 * Description:
 * Log a message locally, then try to send it to the UI.
 *========================================================================
 */
void
_piboxLogger( const char *func, const char *file, int line, int type, const char *fmt, ... )
{
    va_list         ap;
    static char     msgBuf[MAXBUF];

    /* Clamp the type, to avoid errors. */
    if ( (type <= LOG_MIN) && (type >= LOG_MAX) )
        type = 1;

    if ( fmt == NULL )
    {
        _piboxLoggerLocal( func, file, line, type, "Format is null");
        return;
    }

    /* Save a copy of the formatted data. */
    memset(msgBuf, 0, MAXBUF);
    va_start(ap, fmt);
    vsnprintf(msgBuf, MAXBUF-1, fmt, ap);
    va_end(ap);

    /* Log it locally */
    _piboxLoggerLocal( func, file, line, type, "%s", msgBuf );
}
