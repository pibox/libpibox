/*******************************************************************************
 * PiBox library tools
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include "cli.h"

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* -d: Generic data. */
            case 'd':
                cliOptions.data = optarg;
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    memset(&cliOptions, 0, sizeof(CLI_T));
}

/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    cliOptions.flags |= bits;
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    cliOptions.flags &= ~bits;
}

