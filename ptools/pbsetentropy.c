/*******************************************************************************
 * PiBox library tools
 *
 * pbsetentropy.c:  Tool that bumps the available entropy count.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PBSETENTROPY_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/random.h>

#include "log.h"
#include "cli.h"

#define DEVICE  "/dev/urandom"

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main( int argc, char **argv )
{
    char    *name = NULL;
    int     fd;
    int     rc;
    int     size = 0;

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);

    if ( cliOptions.data == NULL )
    {
        piboxLogger(LOG_ERROR, "You must specify the size with -d\n");
        exit(1);
    }
    size = atoi(cliOptions.data);

    fd = open(DEVICE, O_RDONLY);
    if ( fd == -1 )
    {
        piboxLogger(LOG_ERROR, "Failed to open %s: %s\n", DEVICE, strerror(errno));
    }
    else
    {
        rc = ioctl(fd, RNDADDTOENTCNT, &size);
        if ( rc != 0 )
        {
            piboxLogger(LOG_ERROR, "Failed ioctl: %s\n", strerror(errno));
        }
        else
        {
            piboxLogger(LOG_INFO, "Updated entropy count by %d\n", size);
        }
        close(fd);
    }

    piboxLoggerShutdown();
}
