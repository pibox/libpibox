/*******************************************************************************
 * PiBox library tools
 *
 * pbeventlist.c:  Tool that lists available event devices.
 * Parts copied from evtest.c.
 * https://cgit.freedesktop.org/evtest/tree/evtest.c
 *
 * License: see LICENSE file (GPL)
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PBEVENTLIST_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <linux/input.h>

#define DEV_INPUT_EVENT "/dev/input"
#define EVENT_DEV_NAME "event"

/**
 * Filter for the AutoDevProbe scandir on /dev/input.
 *
 * @param dir The current directory entry provided by scandir.
 *
 * @return Non-zero if the given directory entry starts with "event", or zero
 * otherwise.
 */
static int is_event_device(const struct dirent *dir) {
	return strncmp(EVENT_DEV_NAME, dir->d_name, 5) == 0;
}

/*
 * Copied from scan_devices() in evtest.c.
 * https://cgit.freedesktop.org/evtest/tree/evtest.c
 */
static void scan_devices(void)
{
	struct dirent **namelist;
	int i, ndev, devnum, match;
	char *filename;
	int max_device = 0;

	ndev = scandir(DEV_INPUT_EVENT, &namelist, is_event_device, alphasort);
	if (ndev <= 0)
    {
	    fprintf(stderr, "No event devices found.\n");
		return;
    }

	// fprintf(stderr, "Available devices:\n");

	for (i = 0; i < ndev; i++)
	{
		char fname[4096];
		int fd = -1;
		char name[256] = "???";

		snprintf(fname, sizeof(fname),
			 "%s/%s", DEV_INPUT_EVENT, namelist[i]->d_name);
		fd = open(fname, O_RDONLY);
		if (fd < 0)
			continue;
		ioctl(fd, EVIOCGNAME(sizeof(name)), name);

		printf("%s:%s\n", fname, name);
		close(fd);

		match = sscanf(namelist[i]->d_name, "event%d", &devnum);
		if (match >= 1 && devnum > max_device)
			max_device = devnum;

		free(namelist[i]);
	}
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * This just prints a list of event device files and their associated names.
 * ========================================================================
 */
int
main( int argc, char **argv )
{
    char    *name = NULL;

    scan_devices();
}
