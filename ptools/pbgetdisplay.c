/*******************************************************************************
 * PiBox library tools
 *
 * pbgetdisplay.c:  Tool that checks what piboxlib thinks the display is.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PBGETDISPLAY_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <glib.h>

#include "touchProcessor.h"
#include "pibox.h"
#include "utils.h"
#include "log.h"
#include "cli.h"

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main( int argc, char **argv )
{
    int     displayType = 0;
    char    *name = NULL;

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    piboxLoadEnv(NULL);

    /* Call the API function to get display type. */
    displayType = piboxGetDisplayType();
    printf("Display Type: ");
    switch(displayType)
    {
        case PIBOX_LCD:  printf("LCD\n"); break;
        case PIBOX_TFT:  printf("TFT\n"); break;
        case PIBOX_HDMI: printf("HDMI\n"); break;
        case PIBOX_UNKNOWN: printf("Unknown\n"); break;
    }

    piboxLoggerShutdown();
}
