/*******************************************************************************
 * PiBox library tools
 *
 * pbtouchstate.c:  Tool that tests if we're on a supported touchscreen device.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PBTOUCHSTATE_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <glib.h>

#include "touchProcessor.h"
#include "utils.h"
#include "log.h"
#include "cli.h"

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main( int argc, char **argv )
{
    char    *name = NULL;

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    piboxLoadEnv(NULL);

    /* Call the API function to get touchscreen name. */
    name = piboxTouchGetDeviceName();
    if ( name != NULL )
        printf( "%s\n", name );
    else
        printf( "No touchscreen found\n");

    piboxLoggerShutdown();
}
