/*******************************************************************************
 * PiBox application library
 *
 * ptests.h:  Header for test harness
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PTESTS_H
#define PTESTS_H

/*========================================================================
 * Defined values
 *=======================================================================*/
#define CONFIG_TXT_F        "data/config.txt"
#define CONFIG_TXT_BF       "data/xconfig.txt"
#define CONFIG_TXT_BUF      "data/config.txt.backup"

#define MAX_BUF 1024

// Variation identifiers
#define V_ANY               0
#define V_GOOD              1
#define V_BAD               2

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/
#define DISPLAY_ROTATE_DEFAULT  "0x00000"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef FRAMEWORK_C
extern void fwTitle( char *msg );
extern void fwMsg( char *msg );
extern void fwState(int state, const char *fmt, ...);
#endif /* !FRAMEWORK_C */

#ifndef TRPI_C
extern int      TrpiInit( int );
extern int      TrpiSetValue( int );
extern int      TrpiNames( int );
extern int      TrpiGetValue( int );
extern int      TrpiSave( int );
#endif /* !TRPI_C */

#ifndef TLOG_C
extern int      TLoggerInit( int );
extern int      TLoggerVerbosity( int );
extern int      TLogger( int );
extern int      TFormat( int );
#endif /* !TLOG_C */

#ifndef TUTIL_C
extern int      TpiboxTrim( int variation );
extern int      TpiboxStripNewline( int variation );
extern int      TpiboxValidIPAddress( int variation );
extern int      TpiboxToLower( int variation );
extern int      TpiboxGetVTNum( int variation );
extern int      TpiboxGetDisplaySize( int variation );
#endif /* !TUTIL_C */

#ifndef TPMSG_C
extern int      Tpmsg( int );
#endif /* !TPMSG_C */

#ifndef TTOUCHSCREEN_C
extern int      TtouchProcessor( int variation );
extern int      TtouchProcessorDeviceName( int variation );
#endif /* !TTOUCHSCREEN_C */

#endif /* !RPI_H */
