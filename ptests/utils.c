/*******************************************************************************
 * PiBox application library Tests
 *
 * utils.c:  Testing Raspberry Pi utility API.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TUTIL_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <glib.h>

#include "utils.h"
#include "ptests.h"

/*
 *========================================================================
 *========================================================================
 * Private functions
 *========================================================================
 *========================================================================
 */


/*
 *========================================================================
 *========================================================================
 * Public functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   TpiboxTrim
 * Prototype:  int TpiboxTrim( int )
 *
 * Description:
 * Tests trim function in the pibox library.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TpiboxTrim( int variation )
{
    char buf[] = "  data";
    char buf2[] = "\0";
    char *goodPtr = (char *)(buf + 2);
    char *testPtr;

    fwTitle("util");
    fwMsg("piboxTrim: ");
    testPtr = piboxTrim( buf );
    if (testPtr != goodPtr) 
    {
        fwState(0, "Failed to trim leading whitespace correctly; wanted 0x%08x, got 0x%08x", goodPtr, testPtr);
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxTrim - NULL arg: ");
    testPtr = piboxTrim( NULL );
    if (testPtr != NULL) 
    {
        fwState(0, "Failed to catch NULL argument.\n");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxTrim - Empty string: ");
    testPtr = piboxTrim( buf2 );
    if (testPtr != buf2 ) 
    {
        fwState(0, "Failed to catch empty string argument.\n");
        return(1);
    }
    fwState(1, NULL);
    return(0);
}

/*
 *========================================================================
 * Name:   TpiboxStripNewline
 * Prototype:  int TpiboxStripNewline( int )
 *
 * Description:
 * Tests strip newline function in the pibox library.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TpiboxStripNewline( int variation )
{
    char buf[] = "  data\n";
    char buf2[] = "abc";
    char *ptr;

    fwTitle("util");
    fwMsg("piboxStripNewline: ");
    ptr = strdup(buf);
    piboxStripNewline( ptr );
    if ( index(ptr, '\n') != NULL) 
    {
        fwState(0, "Failed to strip newline.\n");
        free(ptr);
        return(1);
    }
    free(ptr);
    fwState(1, NULL);

    // This one would segfault if not caught properly.
    fwMsg("piboxStripNewline - NULL arg: ");
    piboxStripNewline( NULL );
    fwState(1, NULL);

    fwMsg("piboxStripNewline - no newline: ");
    ptr = strdup(buf2);
    piboxStripNewline( ptr );
    if ( strlen(ptr) != 3 ) 
    {
        fwState(0, "Failed to handle argument with no newline.\n");
        return(1);
    }
    free(ptr);
    fwState(1, NULL);
    return(0);
}

/*
 *========================================================================
 * Name:   TpiboxValidIPAddress
 * Prototype:  int TpiboxValidIPAddress( int )
 *
 * Description:
 * Tests case-lowering function in the pibox library.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TpiboxValidIPAddress( int variation )
{
    char *okIP = "192.168.1.2";
    char *badIP = "xx.168.1.2";
    char *nullIP = NULL;

    fwTitle("util");
    fwMsg("piboxValidIPAddress: ");
    if ( !piboxValidIPAddress(okIP) )
    {
        fwState(0, "Failed to identify valid IP address.\n");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxValidIPAddress - NULL arg: ");
    if ( piboxValidIPAddress(nullIP) )
    {
        fwState(0, "Failed to identify NULL address.\n");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxValidIPAddress - invalid address: ");
    if ( piboxValidIPAddress( badIP ) )
    {
        fwState(0, "Failed to identify invalid IP address.\n");
        return(1);
    }
    fwState(1, NULL);
    return(0);
}

/*
 *========================================================================
 * Name:   TpiboxToLower
 * Prototype:  int TpiboxToLower( int )
 *
 * Description:
 * Tests case-lowering function in the pibox library.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TpiboxToLower( int variation )
{
    char *emptyBuf = NULL;

    char buf1a[] = "abc";
    char buf1b[] = "ABC";

    char buf2a[] = " abc ";
    char buf2b[] = " abc ";

    char buf3a[] = " ab\nc ";
    char buf3b[] = " ab\nc ";

    fwTitle("util");
    fwMsg("piboxToLower: ");
    piboxToLower(buf1b);
    if ( strcmp(buf1a, buf1b) != 0 )
    {
        fwState(0, "Failed.\n");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxToLower with whitespace: ");
    piboxToLower(buf2b);
    if ( strcmp(buf2a, buf2b) != 0 )
    {
        fwState(0, "Failed.\n");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxToLower with newline: ");
    piboxToLower(buf3b);
    if ( strcmp(buf3a, buf3b) != 0 )
    {
        fwState(0, "Failed.\n");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("piboxToLower NULL arg: ");
    piboxToLower(emptyBuf);
    fwState(1, NULL);
    return(0);
}

/*
 *========================================================================
 * Name:   TpiboxGetVTNum
 * Prototype:  int TpiboxGetVTNum( int )
 *
 * Description:
 * Tests function in the pibox library that retrieves the current VT number.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TpiboxGetVTNum( int variation )
{
    int     vtnum;
    char    vtchar[7];
    int     result;
    int     rc=0;
    FILE    *pd;

    fwTitle("util");
    fwMsg("piboxGetVTNum: ");

    pd = popen("cat /sys/class/tty/tty0/active", "r");
    if ( pd == NULL )
    {
        fwState(0, "Failed: getVTNum test setup failed: %s.\n", strerror(errno));
        rc=1;
    }
    else
    {
        fread(vtchar, 7, 1, pd);
        piboxStripNewline( vtchar );
        result = piboxGetVTNum();
        if ( result != atoi((char *)(vtchar + 3)) )
        {
            fwState(0, "Failed: getVTNum(%d) != getVT(%s).\n", result, (char *)(vtchar+3));
            rc=1;
        }
        else
            fwState(1, NULL);
    }
    fclose(pd);
    return(rc);
}

/*
 *========================================================================
 * Name:   TpiboxGetDisplaySize
 * Prototype:  int TpiboxGetDisplaySize( int )
 *
 * Description:
 * Tests function in the pibox library that retrieves the current VT number.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TpiboxGetDisplaySize( int variation )
{
    int     vtnum;
    char    vtchar[7];
    int     result;
    int     rc=0;
    int     x0, y0;
    int     x1, y1;
    FILE    *fd;
    char    dimStr[256];

    fwTitle("util");
    fwMsg("piboxGetDisplaySize: ");

    fd = fopen("/sys/class/graphics/fb0/virtual_size", "r");
    if ( fd == NULL )
    {
        fwState(0, "Failed: getDisplaySize test setup failed: %s.\n", strerror(errno));
        rc=1;
    }
    else
    {
        memset(dimStr, 0, 256);
        if ( fgets(dimStr, 255, fd) == NULL )
        {
            fwState(0, "Failed: getDisplaySize setup failed to read fb sizes.\n");
            rc=1;
        }
        else if (index(dimStr, ',') == NULL )
        {
            fwState(0, "Failed: getDisplaySize setup read bad values from fb.\n");
            rc=1;
        }
        else
        {
            x0 = atoi(strtok(dimStr, ","));
            y0 = atoi(strtok(NULL, ","));

            piboxGetDisplaySize(&x1, &y1);
            if ( (x0 != x1) || (y0 != y1) )
            {
                fwState(0, "Failed: getDisplaySize returned bad values from fb.\n");
                rc = 1;
            }
            else
                fwState(1, NULL);
        }
    }
    fclose(fd);
    return(rc);
}
