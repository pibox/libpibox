/*******************************************************************************
 * PiBox application library Tests
 *
 * cli.h:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define CLI_LOGTOFILE   0x00001    // Enable log to file
#define CLI_INSTRUMENT  0x00002    // Enable instrumentation

typedef struct _cli_t {
    int     flags;      // Enable/disable features
    int     verbose;    // Sets the verbosity level for the application
    char    *logFile;   // Name of local file to write log to
    char    *testList;  // Set of tests to run
} CLI_T;


/*========================================================================
 * Text strings 
 *=======================================================================*/

/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "it:l:v:"
#define USAGE \
"\n\
ptests [ -i | -t num[,num,...] | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -t num[,num,...]    Comma separated list of test numbers to run.  Defaults to all tests.\n\
                        Available tests \n\
                        1: TLoggerInit,               V_ANY  \n\
                        2: TLoggerVerbosity,          V_ANY  \n\
                        3: TLogger,                   V_ANY  \n\
                        4: TFormat,                   V_ANY  \n\
                        5: rpiNames,                  V_BAD  \n\
                        6: rpiInit and rpiShow,       V_ANY  \n\
                        7: rpiNames,                  V_GOOD \n\
                        8: rpiGetValue,               V_ANY  \n\
                        9: rpiSetValue,               V_ANY  \n\
                        10: rpiSave,                  V_ANY  \n\
                        11: piboxTrim,                V_ANY  \n\
                        12: piboxStripNewline,        V_ANY  \n\
                        13: piboxValidIPAddress,      V_ANY  \n\
                        14: piboxToLower,             V_ANY  \n\
                        15: piboxGetVTNum,            V_ANY  \n\
                        16: piboxGetDisplaySize,      V_ANY  \n\
                        17: piboxMsg,                 V_ANY  \n\
                        18: touchProcessor,           V_ANY  \n\
                        19: touchProcessorDeviceName, V_ANY  \n\
                        Where  \n\
                        V_ANY means all tests, normal or negative \n\
                        V_BAD means negative tests \n\
                        V_GOOD means normal tests \n\
    -i                  Enable instrumentation (outputs debug instrumentation data) \n\
    -l filename         Enable local logging to named file \n\
    -v level            Enable verbose output: \n\
                        0: LOG_NONE  (default) \n\
                        1: LOG_ERROR           \n\
                        2: LOG_WARN            \n\
                        3: LOG_INFO            \n\
                        4: LOG_TRACE1          \n\
                        5: LOG_TRACE2          \n\
                        6: LOG_TRACE3          \n\
                        7: LOG_TRACE4          \n\
                        8: LOG_TRACE5          \n\
\n\
"


/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void loadConfig( void );
void parseArgs(int argc, char **argv);
void initConfig( void );
void validateConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void loadConfig( void );
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern void validateConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
