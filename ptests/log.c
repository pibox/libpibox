/*******************************************************************************
 * PiBox application library Tests
 *
 * log.c:  Testing Raspberry Pi logging.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TLOG_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>

#include "log.h"
#include "utils.h"
#include "ptests.h"

#define LOGFILE "/tmp/pibox.log"

/*
 *========================================================================
 *========================================================================
 * Private functions
 *========================================================================
 *========================================================================
 */
static int test_open_file( char *logpath )
{
    struct dirent *entry;
    struct dirent *result;
    char path[NAME_MAX+1];
    char buf[NAME_MAX+1];

    DIR *d = opendir("/proc/self/fd");
    if (d) 
    {
        while ( (entry=readdir(d)) != NULL ) 
        {
            if (isdigit(entry->d_name[0])) 
            {
                snprintf(path, sizeof(path), "/proc/self/fd/%s", entry->d_name);
                ssize_t bytes = readlink(path, buf, sizeof(buf));
                buf[bytes] = '\0';
                if (strcmp(logpath, buf) == 0) 
                    break;
            }
        }
        closedir(d);
        if (entry) 
            return 1;
    }
    return 0;
}

static int log_found( char *needle, char *haystack )
{
    int rc;
    char buf[1024];

    sprintf(buf, "grep %s %s >/dev/null 2>/dev/null", needle, haystack);
    rc = system(buf);
    if ( (rc != -1) && (rc != 127) )
        return WEXITSTATUS(rc);
    return 1;
}

/*
 *========================================================================
 *========================================================================
 * Public functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   TLoggerInit
 * Prototype:  int TLoggerInit( int )
 *
 * Description:
 * Tests the piboxLoggerInit() and piboxLoggerShutdown functions.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TLoggerInit( int variation )
{
    int rc = 0;
    struct stat stat_buf;

    fwTitle("piboxLoggerInit/Shutdown");
    fwMsg("NULL dest: ");
    piboxLoggerInit(NULL);
    piboxLoggerShutdown();
    fwState(1, NULL);

    fwMsg("File /tmp/pibox.log: ");
    piboxLoggerInit("/tmp/pibox.log");
    if ( stat( "/tmp/pibox.log", &stat_buf) != 0 )
    {
        fwState(0, "Log file not created.");
        rc = 1;
    }
    piboxLoggerShutdown();
    if ( test_open_file( "/tmp/pibox.log" ) )
    {
        fwState(0, "Log file not cleaned up.");
        rc = 1;
    }
    if ( rc == 0 )
        fwState(1, NULL);
    return(rc);
}

/*
 *========================================================================
 * Name:   TLoggerVerbosity
 * Prototype:  int TLoggerVerbosity( int )
 *
 * Description:
 * Tests the piboxLoggerVerbosity() and piboxLoggerGetVerbosity functions.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 * 
 * 
 *========================================================================
 */
int
TLoggerVerbosity( int variation )
{
    int verbose;
    int rc = 0;

    fwTitle("piboxLoggerVerbosity");
    fwMsg("Init test: ");
    verbose = piboxLoggerGetVerbosity();
    if ( verbose != -1 )
    {
        fwState(0, "Verbosity not initialized; wanted -1, got %d", verbose);
        rc = 1;
    }
    else
        fwState(1, NULL);

    fwMsg("Set test: ");
    piboxLoggerVerbosity(3);
    verbose = piboxLoggerGetVerbosity();
    if ( verbose != 3 )
    {
        fwState(0, "Verbosity not set correctly; wanted 3, got %d", verbose);
        rc = 1;
    }
    else
        fwState(1, NULL);

    fwMsg("Reset test: ");
    piboxLoggerInit(NULL);
    verbose = piboxLoggerGetVerbosity();
    if ( verbose != -1 )
    {
        fwState(0, "Verbosity not initialized to zero.");
        rc = 1;
    }
    else
        fwState(1, NULL);
    piboxLoggerShutdown();
    return(rc);
}

/*
 *========================================================================
 * Name:   TLogger
 * Prototype:  int TLogger( int )
 *
 * Description:
 * Tests the piboxLogger() and piboxLoggerLocal functions.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 * 
 * 
 *========================================================================
 */
int
TLogger( int variation )
{
    int num;
    int rc = 0;
    char buf[128];

    fwTitle("piboxLogger");
    fwMsg("No log file: ");
    piboxLoggerInit(NULL);
    piboxLogger(LOG_INFO, "Test log message\n");
    piboxLoggerShutdown();

    fwMsg("Log file: /tmp/pibox.log ");
    num = random();
    piboxLoggerInit(LOGFILE);
    sprintf(buf, "id %d", num);
    piboxLogger(LOG_INFO, buf);
    if ( !log_found(buf, LOGFILE) )
    {
        fwState(0, "Log message missing from log file.");
        rc = 1;
    }
    piboxLoggerShutdown();
    fwState(1, NULL);
    unlink(LOGFILE);

    return(rc);
}

/*
 *========================================================================
 * Name:   TFormat
 * Prototype:  int TFormat( int )
 *
 * Description:
 * Tests the format of the log from piboxLogger() and piboxLoggerLocal() functions.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TFormat( int variation )
{
    int     num;
    int     rc = 0;
    char    hdr[128];
    char    type[16];
    char    msg[128];
    FILE    *fd;
    time_t  timestamp;

    unlink(LOGFILE);
    fwTitle("piboxLogger");
    fwMsg("format test: ");
    piboxLoggerSetFlags(LOG_F_TIMESTAMP | LOG_F_FIXEDHDR);
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_INFO);
    piboxLogger(LOG_INFO, "Test log message\n");
    piboxLoggerShutdown();

    memset(hdr, 0, 128);
    memset(type, 0, 16);
    memset(msg, 0, 128);
    fd = fopen(LOGFILE, "r");
    fscanf(fd, "%08x", &timestamp);
    fseek(fd, 1, SEEK_CUR);
    fgets(hdr, 21, fd);
    fscanf(fd, "%s ", type);
    fgets(msg, 128, fd);
    piboxStripNewline(msg);
    fclose(fd);
    unlink(LOGFILE);

#if 0
    fprintf( stderr,
            "Timestamp          : %08x\n"
            "Header             : %s\n"
            "Type               : %s\n"
            "Payload            : %s\n",
            timestamp,
            hdr,
            type,
            msg);
#endif

    if ( strlen(hdr) != 20 )
    {
        fwState(0, "Incorrect fixed size header; wanted 20, got %d; hdr=%s\n", strlen(hdr), hdr);
        return(1);
    }
    if ( strcmp(type, "INFO") != 0 )
    {
        fwState(0, "Incorrect log type; wanted LOG_INFO, got %s\n", type);
        return(1);
    }
    if ( strcmp(msg, "Test log message") != 0 )
    {
        fwState(0, "Incorrect log message; got *%s*\n", msg);
        return(1);
    }

    fwState(1, NULL);
    return(rc);
}

