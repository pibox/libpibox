/*******************************************************************************
 * PiBox application library Tests
 *
 * pmsg.c:  Message Flow Protocol tests
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TPMSG_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <glib.h>

#include "pibox.h"
#include "pmsg.h"
#include "log.h"
#include "ptests.h"

#define LOGFILE     "/tmp/pmsg.log"
#define UUID        "0e92b74c-e7ff-11e5-ba13-0011506f6a67"

/*========================================================================
 * Private Functions
 *========================================================================*/

/*
 *========================================================================
 * Name:   dotToDash
 * Prototype:  void dotToDash( char * )
 *
 * Description:
 * Convert "." to "-" in the supplied buffer.
 *========================================================================
 */
void
dotToDash( char *buf )
{
    char *ptr = buf;
    while (*ptr != '\0' )
    {   
        if (*ptr == '.')
            *ptr = '-';
        ptr++;
    }
}

/*========================================================================
 * Name:   getIP
 * Prototype:  int getIP( char *buf )
 *
 * Description:
 * Retrieve the first IP address configured.  If multiple are configured, 
 * it may not be what you're looking for.
 *
 * Returns:
 * 0 on success.
 * 1 on failure.
 *
 * The IP address is copied into buf.  The caller is responsible for making
 * sure this is big enough to hold the string.
 *========================================================================*/
static int
getIP( char *buf )
{
    struct ifaddrs *ifaddr, *ifa;
    int family, rc;
    char host[NI_MAXHOST];
    int match = 0;

    if (getifaddrs(&ifaddr) == -1)
    {                                                                                                                 
        piboxLogger(LOG_ERROR, "Failed to get my own IP addresses.");
        return 0;
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        rc=-1;
        family = ifa->ifa_addr->sa_family;
        if (family == AF_INET || family == AF_INET6)
        {
            // This is how we get the character string of the IP address.
            rc = getnameinfo(ifa->ifa_addr,
                           (family == AF_INET) ? sizeof(struct sockaddr_in) :
                                                 sizeof(struct sockaddr_in6),
                           host, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
        }

        if ( (rc == 0) && (strncmp(host, "127", 3) != 0) )
        {
            // We really only go through this loop once.
            strcpy(buf, host);
            return (0);
        }
    }
    return (1);
}

/*========================================================================
 * Name:   testLog
 * Prototype:  int testLog( guint8 level, const char *fmt, ... )
 *
 * Description:
 * Check if the specified message is in the log file.
 *
 * Returns:
 * The number of times the message appears or -1 on error.
 *========================================================================*/
static int
testLog( guint8 level, const char *fmt, ... )
{
    va_list ap;
    char    buf[MAX_BUF];
    FILE    *pd;
    char    cmd[MAX_BUF+100];
    char    result[128];

    memset(buf, 0, MAX_BUF);
    va_start (ap, fmt);
    vsnprintf(buf, MAX_BUF-1, fmt, ap);
    va_end (ap);

    sprintf(cmd, "grep \"%s\" %s 2>/dev/null | wc -l", buf, LOGFILE); 
    pd = popen(cmd, "r");
    if (pd == NULL)
    {
        return -1;
    }
    if ( fgets(result, 128, pd) == NULL )
    {
        fclose(pd);
        return -1;
    }
    fclose(pd);
    return atoi(result);
}

/*========================================================================
 * Name:   catLog
 * Prototype:  void catLog( void )
 *
 * Description:
 * Check if the specified message is in the log file.
 *
 * Returns:
 * The number of times the message appears or -1 on error.
 *========================================================================*/
static void
catLog( void )
{
    char    cmd[MAX_BUF+100];

    fprintf(stderr, "\n----------------------------\n"); 
    fprintf(stderr, "Log file: \n"); 
    sprintf(cmd, "cat %s", LOGFILE); 
    system(cmd);
    fprintf(stderr, "----------------------------\n"); 
}

/*========================================================================
 * Name:   pmsgBadType
 * Prototype:  int pmsgBadType( int )
 *
 * Description:
 * Verify failures when specifying a bad types.
 *========================================================================*/
static int
pmsgBadType( int bits )
{
    char    *payload="192.168.101.1";
    char    *result=NULL;
    guint   resultSize=0;
    int     rc;
    int     cnt;

    fwMsg("Bad type: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(255, MA_GET, 0, NULL, 0, payload, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 0 )
        fwState(0, "Processed bad type.\n");
    else
    {
        cnt = testLog(LOG_ERROR, "Invalid type and/or action specified: %d, %d", 255, MA_GET);
        if ( cnt != 1 )
        {
            fwState(0, "Failed to identify bad type; wanted 1, got %d\n", cnt);
            catLog();
            return(1);
        }
        else
            fwState(1, NULL);
    }
    unlink(LOGFILE);

    fwMsg("Unsupported type: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(MT_KEY, MA_GET, 0, NULL, 0, payload, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 0 )
        fwState(0, "Processed unsupported type.\n");
    else
    {
        if ( testLog(LOG_ERROR, "Invalid type and/or action specified: %d, %d", MT_KEY, MA_GET) != 1 )
        {
            fwState(0, "Failed to identify unsupported type.\n");
            catLog();
            return(1);
        }
        else
            fwState(1, NULL);
    }
    unlink(LOGFILE);
    return(0);
}

/*========================================================================
 * Name:   pmsgBadAction
 * Prototype:  int pmsgBadType( int )
 *
 * Description:
 * Verify failures when specifying a bad actions.
 *========================================================================*/
static int
pmsgBadAction( int bits )
{
    char    *payload="192.168.101.1";
    char    *result=NULL;
    guint   resultSize=0;
    int     rc;
    int     cnt;

    fwMsg("Bad action: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(MT_DEVICE, 255, 0, NULL, 0, payload, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 0 )
        fwState(0, "Processed bad action.\n");
    else
    {
        cnt = testLog(LOG_ERROR, "Invalid type and/or action specified: %d, %d", MT_DEVICE, 255);
        if ( cnt != 1 )
        {
            fwState(0, "Failed to identify bad action; wanted 1, got %d\n", cnt);
            catLog();
            return(1);
        }
        else
            fwState(1, NULL);
    }
    unlink(LOGFILE);

    fwMsg("Unsupported action: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(MT_DEVICE, MA_RESTART, 0, NULL, 0, payload, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 0 )
        fwState(0, "Processed unsupported action.\n");
    else
    {
        if ( testLog(LOG_ERROR, "Invalid type and/or action specified: %d, %d", MT_DEVICE, MA_RESTART) != 1 )
        {
            fwState(0, "Failed to identify unsupported action.\n");
            catLog();
            return(1);
        }
        else
            fwState(1, NULL);
    }
    unlink(LOGFILE);
    return(0);
}

/*========================================================================
 * Name:   pmsgMissingPayload
 * Prototype:  int pmsgMissingPayload( int )
 *
 * Description:
 * Verify failures when specifying a bad actions.
 *========================================================================*/
static int
pmsgMissingPayload( int bits )
{
    char    *payload=NULL;
    char    *result=NULL;
    guint   resultSize=0;
    int     rc;
    int     cnt;

    fwMsg("Missing payload: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(MT_DEVICE, MA_GET, 0, NULL, 0, payload, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 0 )
        fwState(0, "Processed with missing payload.\n");
    else
    {
        cnt = testLog(LOG_ERROR, "Missing required payload\n");
        if ( cnt != 1 )
        {
            fwState(0, "Failed to catch missing payload buffer.\n");
            catLog();
            return(1);
        }
        else
            fwState(1, NULL);
    }
    unlink(LOGFILE);
    return(0);
}

/*========================================================================
 * Name:   pmsgMissingReturn
 * Prototype:  int pmsgMissingReturn( int )
 *
 * Description:
 * Verify failures when return buffer is missing.
 *========================================================================*/
static int
pmsgMissingReturn( int bits )
{
    char    *payload="this is a test";
    char    *result=NULL;
    guint   resultSize=0;
    int     rc;
    int     cnt;

    fwMsg("Missing return buffer: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(MT_DEVICE, MA_GET, 0, NULL, strlen(payload), payload, &resultSize, NULL);
    piboxLoggerShutdown();
    if ( rc == 0 )
        fwState(0, "Processed with missing return buffer.\n");
    else
    {
        cnt = testLog(LOG_ERROR, "Missing required return buffer\n");
        if ( cnt != 1 )
        {
            fwState(0, "Failed to catch missing return buffer.\n");
            catLog();
            return(1);
        }
        else
            fwState(1, NULL);
    }
    unlink(LOGFILE);
    return(0);
}

/*========================================================================
 * Name:   pmsgGetAll
 * Prototype:  int pmsgGetAll( int )
 *
 * Description:
 * Validate return from GET_ALL request.
 *
 * Notes:
 * Requires ncat server be started before this test to respond to request.
 *========================================================================*/
static int
pmsgGetAll( int bits )
{
    char    *result=NULL;
    guint   resultSize=0;
    int     rc = 0;
    int     tRC = 0;
    int     cnt;

    fwMsg("GET_ALL: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    rc = piboxMsg(MT_DEVICE, MA_GETALL, 0, NULL, 0, NULL, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 1 )
    {
        cnt = testLog(LOG_ERROR, "Failed to get a socket\n");
        if ( cnt == 1 )
        {
            fwState(0, "Server isn't running - can't run this test.\n");
            tRC = 1;
            goto getAllExit;
        }
    }
    else
    {
        if ( strstr(result, UUID) == NULL )
        {
            fwState(0, "Return data missing uuid (%s): %s\n", UUID, result);
            tRC = 1;
        }
        else
            fwState(1, NULL);
    }

getAllExit:
    unlink(LOGFILE);
    return(rc);
}

/*========================================================================
 * Name:   pmsgGet
 * Prototype:  int pmsgGet( int )
 *
 * Description:
 * Validate return from GET request.
 *
 * Notes:
 * Requires ncat server be started before this test to respond to request.
 *========================================================================*/
static int
pmsgGet( int bits )
{
    char    payload[NI_MAXHOST];
    char    *result=NULL;
    guint   resultSize=0;
    int     rc;
    int     tRC = 0;
    int     cnt = 0;

    fwMsg("GET device: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);
    memset(payload, 0, NI_MAXHOST);
    if ( getIP(payload) )
    {
        piboxLoggerShutdown();
        fwState(0, "Failed to get local IP - can't run this test.\n");
        goto getExit;
    }
    dotToDash(payload);
    piboxLogger(LOG_INFO, "Payload: %s\n", payload);
    rc = piboxMsg(MT_DEVICE, MA_GET, 0, NULL, strlen(payload), payload, &resultSize, &result);
    piboxLoggerShutdown();
    if ( rc == 1 )
    {
        cnt = testLog(LOG_ERROR, "Failed to get a socket\n");
        if ( cnt == 1 )
        {
            fwState(0, "Server isn't running - can't run this test.\n");
            goto getExit;
        }
    }
    else
    {
        if ( strstr(result, UUID) == NULL )
        {
            fwState(0, "Return data missing UUID (%s): %s\n", UUID, payload);
            tRC = 1;
        }
        else
            fwState(1, NULL);
    }

getExit:
    unlink(LOGFILE);
    return(0);
}

/*========================================================================
 * Name:   pmsgSet
 * Prototype:  int pmsgSet( int )
 *
 * Description:
 * Validate return from SET request.
 *
 * Notes:
 * Requires ncat server be started before this test to respond to request.
 *========================================================================*/
static int
pmsgSet( int bits )
{
    char    payload[256];
    char    *result=NULL;
    guint   resultSize=0;
    char    *ptr=NULL;
    int     rc;
    int     tRC = 0;
    int     cnt;
    int     value;

    fwMsg("GET device: ");
    piboxLoggerInit(LOGFILE);
    piboxLoggerVerbosity(LOG_TRACE1);

    memset(payload, 0, 256);
    if ( getIP(payload) )
    {
        piboxLoggerShutdown();
        fwState(0, "Failed to get local IP - can't run this test.\n");
        tRC = 1;
        goto setExit;
    }
    dotToDash(payload);
    rc = piboxMsg(MT_DEVICE, MA_GET, 0, NULL, strlen(payload), payload, &resultSize, &result);
    if ( rc == 1 )
    {
        cnt = testLog(LOG_ERROR, "Failed to get a socket\n");
        if ( cnt == 1 )
        {
            fwState(0, "Server isn't running - can't run this test.\n");
            tRC = 1;
            goto setExit;
        }
    }

    // Update the result so we can send it back.
    // Find the current value for "damper-set".  This is a cheap method without breaking into fields.
    ptr = strstr(result, "damper-set");
    if ( ptr == NULL )
    {
        fwState(0, "Missing damper-set field in return data.\n");
        tRC = 1;
        goto setExit;
    }
    while ( *ptr != ':' )
        ptr++;
    ptr += 2;

    // Toggle the value
    if ( *ptr == '0' )
    {
        value = 1;
        *ptr = '1';
    }
    else
    {
        value = 0;
        *ptr = '0';
    }

    strcat(payload, "\n");
    strcat(payload, result);
    piboxLogger(LOG_INFO, "Updated payload: %s\n", payload);
    rc = piboxMsg(MT_DEVICE, MA_SET, 0, NULL, strlen(payload), payload, &resultSize, NULL);
    if ( rc == 1 )
    {
        cnt = testLog(LOG_ERROR, "Failed to get a socket\n");
        if ( cnt == 1 )
        {
            fwState(0, "Server isn't running - can't run this test.\n");
            tRC = 1;
            goto setExit;
        }
    }

    // Now retrieve the packet again to check it was updated.
    memset(payload, 0, 256);
    if ( getIP(payload) )
    {
        piboxLoggerShutdown();
        fwState(0, "Failed to get local IP - can't run this test.\n");
        tRC = 1;
        goto setExit;
    }
    dotToDash(payload);
    rc = piboxMsg(MT_DEVICE, MA_GET, 0, NULL, strlen(payload), payload, &resultSize, &result);
    if ( rc == 1 )
    {
        cnt = testLog(LOG_ERROR, "Failed to get a socket\n");
        if ( cnt == 1 )
        {
            fwState(0, "Server isn't running - can't run this test.\n");
            tRC = 1;
            goto setExit;
        }
    }

    // check the updated value
    ptr = strstr(result, "damper-set");
    if ( ptr == NULL )
    {
        fwState(0, "Missing damper-set field in return data.\n");
        tRC = 1;
        goto setExit;
    }
    while ( *ptr != ':' )
        ptr++;
    ptr += 2;

    if ( (value == 0) && (*ptr != '0') )
    {
        fwState(0, "0: Update didn't take.\n");
        tRC = 1;
        goto setExit;
    }
    else if ( (value == 1) && (*ptr != '1') )
    {
        fwState(0, "1: Update didn't take.\n");
        tRC = 1;
        goto setExit;
    }

setExit:
    piboxLoggerShutdown();
    unlink(LOGFILE);
    if ( tRC == 0 )
        fwState(1, NULL);
    return(tRC);
}

/*========================================================================
 * Public Functions
 *========================================================================*/

/*========================================================================
 * Name:   Tpmsg
 * Prototype:  int Tpmsg( int )
 *
 * Description:
 * Run the various piboxMsg tests.
 *========================================================================*/
int
Tpmsg( int bits )
{
    int rc = 0;

    fwTitle("piboxMsg");

    rc=pmsgBadType(bits);
    if ( rc != 0 )
        return(rc);

    rc=pmsgBadAction(bits);
    if ( rc != 0 )
        return(rc);

    rc= pmsgMissingPayload(bits);
    if ( rc != 0 )
        return(rc);

    rc= pmsgMissingReturn(bits);
    if ( rc != 0 )
        return(rc);

    rc= pmsgGetAll(bits);
    if ( rc != 0 )
        return(rc);

    rc= pmsgGet(bits);
    if ( rc != 0 )
        return(rc);

    rc= pmsgSet(bits);
    if ( rc != 0 )
        return(rc);

    return 0;
}
