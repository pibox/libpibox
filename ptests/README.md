The unit tests for libpibox must be run from this directory (ptests) in order to access tests data.
To get usage information, run

  ./ptests -?

An example run

  ./ptests -v3 -t12,13,14

For some tests the piboxd daemon must be running.
