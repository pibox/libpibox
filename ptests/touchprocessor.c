/*******************************************************************************
 * PiBox application library Tests
 *
 * touchscreen.c:  Testing Raspberry Pi touchscreen support.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TTOUCHSCREEN_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>

#include "touchProcessor.h"
#include "log.h"
#include "utils.h"
#include "ptests.h"

/*
 *========================================================================
 *========================================================================
 * Private functions
 *========================================================================
 *========================================================================
 */
gboolean 
absTouch( REGION_T *region )
{
    piboxLogger(LOG_INFO, "Touch press ABS coords: %d / %d \n", region->x, region->y);
}

/*
 *========================================================================
 *========================================================================
 * Public functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   TtouchProcessor
 * Prototype:  int TtouchProcessor( int )
 *
 * Description:
 * Tests the Touch Screen functions.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TtouchProcessor( int variation )
{
    int rc = 1;
    struct stat stat_buf;

    fwTitle("Touchscreen ABS");

    /* Enable ABS touchscreen for 5 seconds. */
    piboxLoggerInit(NULL);
    piboxLoggerVerbosity(3);
    piboxTouchRegisterCB(absTouch, TOUCH_ABS);
    piboxTouchStartProcessor();
    sleep(10);
    piboxTouchShutdownProcessor();
    piboxLoggerShutdown();

    fwMsg("Done. ");
    fwState(rc, NULL);

    return(rc);
}

/*
 *========================================================================
 * Name:   TtouchProcessorDeviceName
 * Prototype:  int TtouchProcessorDeviceName( int )
 *
 * Description:
 * Retrieve the touch device name, if any.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TtouchProcessorDeviceName( int variation )
{
    int rc = 1;
    struct stat stat_buf;
    char *name = NULL;

    piboxTouchRegisterCB(absTouch, TOUCH_ABS);
    piboxTouchStartProcessor();
    usleep(250000);

    name = piboxTouchGetDeviceName();
    if ( name != NULL )
        fwMsg(name);
    else
        fwMsg("Not found");

    piboxTouchShutdownProcessor();
    piboxLoggerShutdown();

    fwState(rc, NULL);
    return(rc);
}
