/*******************************************************************************
 * PiBox application library Tests
 *
 * main.c:  Front end for the unit tests
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MAIN_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <glib.h>

#include "rpi.h"
#include "log.h"
#include "ptests.h"
#include "cli.h"

typedef struct _test_entry {
    char    *id;
    int     (*func)(int);
    int     arg;
} TEST_ENTRY;

TEST_ENTRY tests[] = {
    // Log API
    {"1", TLoggerInit,        V_ANY},
    {"2", TLoggerVerbosity,   V_ANY},
    {"3", TLogger,            V_ANY},
    {"4", TFormat,            V_ANY},

    // RPI API
    {"5", TrpiNames,          V_BAD },
    {"6", TrpiInit,           V_ANY},
    {"7", TrpiNames,          V_GOOD},
    {"8", TrpiGetValue,       V_ANY},
    {"9", TrpiSetValue,       V_ANY},
    {"10", TrpiSave,          V_ANY},

    // Utils API
    {"11", TpiboxTrim,          V_ANY},
    {"12", TpiboxStripNewline,  V_ANY},
    {"13", TpiboxValidIPAddress,V_ANY},
    {"14", TpiboxToLower,       V_ANY},
    {"15", TpiboxGetVTNum,      V_ANY},
    {"16", TpiboxGetDisplaySize,V_ANY},

    // piboxMsg API
    {"17", Tpmsg,             V_ANY},

    // touchProcessor API
    {"18", TtouchProcessor,             V_ANY},
    {"19", TtouchProcessorDeviceName,   V_ANY},

    {NULL, NULL},
};

/*
 *========================================================================
 * Name:   main
 * Prototype:  int main( int argc, char **argv )
 *
 * Description:
 * Manage unit tests
 *
 * Returns:
 * 0 on success
 * 1 on error
 *========================================================================
 */
int
main( int argc, char **argv  )
{
    int idx = 0;
    char *tok;
    char *tsave;
    int rc;

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    piboxLoggerInit( cliOptions.logFile );
    piboxLoggerVerbosity( cliOptions.verbose );

    if ( strcmp(cliOptions.testList, "all") == 0 )
    {
        while (tests[idx].id != NULL)
        {
            rc = tests[idx].func( tests[idx].arg );
            if ( rc != 0 )
                exit (rc);
            idx++;
        }
    }
    else
    {
        tok = strtok_r(cliOptions.testList, ",", &tsave);
        while (tok != NULL)
        {
            idx = 0;
            while (tests[idx].id != NULL)
            {
                if ( strcmp(tests[idx].id, tok) == 0 )
                {
                    rc = tests[idx].func( tests[idx].arg );
                    if ( rc != 0 )
                        exit (rc);
                    break;
                }
                idx++;
            }
            tok = strtok_r(NULL, ",", &tsave);
        }
        
    }

    return 0;
}

