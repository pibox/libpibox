/*******************************************************************************
 * PiBox application library Tests
 *
 * rpi.c:  Testing Raspberry Pi config file.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define TRPI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <glib.h>

#include "rpi.h"
#include "log.h"
#include "ptests.h"

static char *rpi_names[] = {
    RPI_DISABLE_OVERSCAN,
    RPI_OVERSCAN_LEFT,
    RPI_OVERSCAN_RIGHT,
    RPI_OVERSCAN_TOP,
    RPI_OVERSCAN_BOTTOM,
    RPI_HDMI_SAVE,
    RPI_HDMI_DRIVE,
    RPI_HDMI_MODE,
    RPI_DECODE_MPG2,
    RPI_DISPLAY_ROTATE,
    RPI_OVERCLOCK,
    RPI_GPU_256,
    RPI_GPU_512,
    RPI_HDMI_IGNORE_EDID_AUDIO,
    RPI_DISABLE_SPLASH,
    RPI_DTPARAM,
    NULL
};

static int overclock_values[] = {
    700,
    800,
    900,
    950,
    1000
};

/*
 *========================================================================
 *========================================================================
 * Private functions
 *========================================================================
 *========================================================================
 */

static void
printNames( gpointer item, gpointer user_data )
{
    RPI_NAME_T *data = (RPI_NAME_T *)item;
    if ( (data->name!=NULL) && (data->value!=NULL) )
        piboxLogger(LOG_INFO, "[FW] Name/Value: %s / %s\n", (char *)data->name, (char *)data->value);
    else
        piboxLogger(LOG_INFO, "[FW] Name/Value is incomplete.\n");
}

static void
validate_config( gpointer item, gpointer user_data )
{
    RPI_NAME_T *entry = (RPI_NAME_T *)item;
    int idx = 0;
    int *rc = (int *)user_data;

    if ( strncmp(entry->name, "#", 1) == 0 )
        return;
    if ( strncmp(entry->name, "NEWLINE", 7) == 0 )
        return;

    while (rpi_names[idx] != NULL)
    {
        if ( strcmp(rpi_names[idx], entry->name) == 0 )
            return;
        idx++;
    }
    fwState(0, "%s from config.txt is not a valid name", entry->name);
    *rc = 1;
}

/*
 *========================================================================
 *========================================================================
 * Public functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   TrpiInit
 * Prototype:  int TrpiInit( int )
 *
 * Description:
 * Tests the rpiInit() function.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TrpiInit( int variation )
{
    GSList *config;
    int idx;
    char buf[MAX_BUF];
    char *value;
    int rc;

    fwTitle("rpiInit");

    fwMsg("rpiInit (NULL file): ");
    if ( rpiInit( NULL ) )
    {
        fwState(0, "NULL file not caught");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("rpiInit (bad file): ");
    if ( rpiInit( CONFIG_TXT_BF ) )
    {
        fwState(0, "Opened non-existent file");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("rpiInit (good file): ");
    if ( !rpiInit( CONFIG_TXT_F ) )
    {
        fwState(0, "Failed to open or read config.txt");
        return(1);
    }
    fwState(1, NULL);

    fwTitle("rpiGetConfig");
    fwMsg("rpiGetConfig: ");
    config = rpiGetConfig();
    if ( g_slist_length(config) == 0 )
    {
        fwState(0, "No elements parsed from config.txt");
        return(1);
    }
    else
        fwState(1, NULL);

    fwTitle("rpiShow");
    sprintf(buf, "rpiShow (entries=%d): ", g_slist_length(config));
    fwMsg(buf);
    rpiShow();
    fwState(1, NULL);

    fwMsg("Validate config entries: ");
    idx=0;
    rc = 0;
    g_slist_foreach(config, validate_config, (gpointer)&rc);
    if ( rc != 0 )
    {
        fwState(0, "Not all config entries are valid.");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("rpiInit (repeat): ");
    if ( rpiInit( CONFIG_TXT_F ) )
    {
        fwState(0, "Multiple init's are not allowed.");
        return(1);
    }
    fwState(1, NULL);
    return 0;
}

/*
 *========================================================================
 * Name:   TrpiNames
 * Prototype:  int TrpiNames( int )
 *
 * Description:
 * Tests the rpiNames() function.
 *
 * Arguments:
 * int variation      Defines which tests to run.
 *                    Valid values are:
 *                    V_BAD
 *                    V_GOOD
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TrpiNames( int variation )
{
    GSList *data;

    fwTitle("rpiNames");
    switch( variation )
    {
        // In this case, we try to get the names before they are initialized.
        case V_BAD:
            fwMsg("Uninitialized: ");
            data = rpiNames();
            if ( data != NULL )
            {
                fwState(0, "rpiNames returned non-NULL when it should be uninitialized.");
                return(1);
            }
            fwState(1, NULL);
            break;

        case V_GOOD:
            fwMsg("Valid: ");
            data = rpiNames();
            g_slist_foreach( data, printNames, NULL);
            fwState(1, NULL);

            fwMsg("Repeat: ");
            data = rpiNames();
            g_slist_foreach( data, printNames, NULL);
            fwState(1, NULL);
            break;
    }
    return 0;
}

/*
 *========================================================================
 * Name:   TrpiGetValue
 * Prototype:  int TrpiGetValue( int )
 *
 * Description:
 * Tests the rpiGetValue() function.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TrpiGetValue( int variation )
{
    GHashTable *data;
    char *value;
    char buf[MAX_BUF];

    fwTitle("rpiGetValue");
    fwMsg("NULL arg: ");
    value = rpiGetValue(NULL);
    if ( value != NULL )
    {
        sprintf(buf, "wanted NULL, got %s", value);
        fwState(0, buf);
        return(1);
    }
    fwState(1, NULL);

    fwMsg("display_rotate: ");
    value = rpiGetValue("display_rotate");
    if ( value == NULL )
    {
        sprintf(buf, "wanted %s, got NULL", DISPLAY_ROTATE_DEFAULT);
        fwState(0, buf);
        return(1);
    }
    if ( strcmp(value, DISPLAY_ROTATE_DEFAULT) != 0 )
    {
        sprintf(buf, "wanted %s, got %s", DISPLAY_ROTATE_DEFAULT, value);
        fwState(0, buf);
        return(1);
    }
    fwState(1, NULL);
    return 0;
}

/*
 *========================================================================
 * Name:   TrpiSetValue
 * Prototype:  int TrpiSetValue( int )
 *
 * Description:
 * Tests the rpiSetValue() function.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TrpiSetValue( int variation )
{
    GHashTable *data;
    char *value;
    char buf[MAX_BUF];
    char ocvalue[6];
    int i;

    fwTitle("rpiSetValue");
    fwMsg("display_rotate (vertical flip): ");
    if ( rpiSetValue("display_rotate", "0x20000") != 0 )
    {
        fwState(0, "");
        return(1);
    }
    value = rpiGetValue("display_rotate");
    if ( value == NULL )
    {
        sprintf(buf, "wanted 0x20000, got NULL");
        fwState(0, buf);
        return(1);
    }
    if ( strcmp(value, "0x20000") != 0 )
    {
        sprintf(buf, "wanted 0x20000, got %s", value);
        fwState(0, buf);
        return(1);
    }
    fwState(1, NULL);
    rpiShow();

    // -----------------------------------------------------------------
    // Negative tests: Try to set values that should fail for each field.
    // -----------------------------------------------------------------

    fwTitle("rpiSetValue (negative tests)");

    // disable_overscan
    fwMsg("disable_overscan (-1): ");
    if ( rpiSetValue(RPI_DISABLE_OVERSCAN, "-1") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("disable_overscan (2): ");
    if ( rpiSetValue(RPI_DISABLE_OVERSCAN, "2") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("disable_overscan (hex): ");
    if ( rpiSetValue(RPI_DISABLE_OVERSCAN, "0xa") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);

    // hdmi_save
    fwMsg("hdmi_save (-1): ");
    if ( rpiSetValue(RPI_HDMI_SAVE, "-1") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("hdmi_save (2): ");
    if ( rpiSetValue(RPI_HDMI_SAVE, "2") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);

    // hdmi_drive
    fwMsg("hdmi_drive (0): ");
    if ( rpiSetValue(RPI_HDMI_DRIVE, "0") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("hdmi_drive (3): ");
    if ( rpiSetValue(RPI_HDMI_DRIVE, "3") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);

    // hdmi_mode
    fwMsg("hdmi_mode (-1): ");
    if ( rpiSetValue(RPI_HDMI_MODE, "-1") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("hdmi_mode (87): ");
    if ( rpiSetValue(RPI_HDMI_MODE, "87") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("hdmi_mode (hex value): ");
    if ( rpiSetValue(RPI_HDMI_MODE, "0xab") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);

    // decode_mpg2
    fwMsg("decode_mpg2(abcdef): ");
    if ( rpiSetValue(RPI_DECODE_MPG2, "abcdef") == 1 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("decode_mpg2(8a654321): ");
    if ( rpiSetValue(RPI_DECODE_MPG2, "8a654321") == 1 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("decode_mpg2(0x1234N678): ");
    if ( rpiSetValue(RPI_DECODE_MPG2, "0x1234N678") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);

    // display_rotate
    fwMsg("display_rotate(00000): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "00000") == 1 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(0000n): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0000n") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(0x0000n): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0x0000n") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(field 1 - 0x30000): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0x30000") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(field 2 - 0x01000): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0x01000") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(field 3 - 0x00100): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0x00100") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(field 4 - 0x00010): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0x00010") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    fwMsg("display_rotate(field 5 - 0x00005): ");
    if ( rpiSetValue(RPI_DISPLAY_ROTATE, "0x00005") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);

    // overclock
    fwMsg("overclock (bad value - 1): ");
    if ( rpiSetValue(RPI_OVERCLOCK, "1") == 0 )
    {
        fwState(0, "");
        return(1);
    }
    fwState(1, NULL);
    for(i=0; i<5; i++)
    {
        sprintf(buf, "overclock (%d): ", overclock_values[i]);
        fwMsg(buf);
        sprintf(ocvalue, "%d", overclock_values[i]);
        if ( rpiSetValue(RPI_OVERCLOCK, ocvalue) == 1 )
        {
            fwState(0, "");
            return(1);
        }
        fwState(1, NULL);
    }

    return 0;
}

/*
 *========================================================================
 * Name:   TrpiSave
 * Prototype:  int TrpiSave( int variation )
 *
 * Description:
 * Tests the rpiSave() function.
 *
 * Arguments:
 * int variation      Unused
 *
 * Returns:
 * 0 on success
 * 1 on error
 * Sets testError on error.
 *========================================================================
 */
int
TrpiSave( int variation )
{
    GHashTable *data;
    char *value;
    char buf[MAX_BUF];

    fwTitle("rpiSave");
    fwMsg("NULL filename: ");
    if ( rpiSave(NULL) != 1 )
    {
        fwState(0, "Didn't catch null filename");
        return(1);
    }
    fwState(1, NULL);

    fwMsg("Valid filename: ");
    if ( rpiSave(CONFIG_TXT_BUF) != 0 )
    {
        fwState(0, "Save to file failed.\n");
        return(1);
    }
    fwState(1, NULL);
    return 0;
}
