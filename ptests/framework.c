/*******************************************************************************
 * PiBox application library Tests
 *
 * framework.c:  Support functions for test framework
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define FRAMEWORK_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <glib.h>

#include "ptests.h"

#define LOG_MIN             0
#define LOG_ERROR           1
#define LOG_WARN            2
#define LOG_INFO            3
#define LOG_TRACE1          4
#define LOG_TRACE2          5
#define LOG_TRACE3          6
#define LOG_TRACE4          7
#define LOG_TRACE5          8

static char *LogType[9] = {
    "UNKNOWN",
    "ERROR",
    "WARN",
    "INFO",
    "TRACE1",
    "TRACE2",
    "TRACE3",
    "TRACE4",
    "TRACE5"
};

static char msgBuf[MAX_BUF];

/*
 *========================================================================
 *========================================================================
 * Private functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 *========================================================================
 * Public functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   fwTitle
 * Prototype:  void fwTitle( char *msg )
 *
 * Description:
 * Display a framework title for a test.
 *========================================================================
 */
void
fwTitle( char *msg )
{
    char buf[MAX_BUF];
    sprintf(buf, "[FW] ============= Test Title: %s\n", msg);
    fprintf(stderr, "%s: %s\n", LogType[LOG_INFO], buf);
}


/*
 *========================================================================
 * Name:   fwMsg
 * Prototype:  void fwMsg( char *msg )
 *
 * Description:
 * Store a message in the message buffer.
 * This clears any previous message from the buffer.
 *========================================================================
 */
void
fwMsg( char *msg )
{
    memset(msgBuf, 0, MAX_BUF);
    sprintf(msgBuf, "[FW] %s", msg);
}

/*
 *========================================================================
 * Name:   fwState
 * Prototype:  void fwState( int state, cost char *fmt, ... )
 *
 * Description:
 * Appends the state string to the message buffer and adds a newline.
 * It then logs the message buffer.
 * 
 * Arguments:
 * state      State to set.  0 is FAIL. 1 is PASS.
 *========================================================================
 */
void
fwState( int state, const char *fmt, ... )
{
    va_list ap;
    char    buf[MAX_BUF];

    memset(buf, 0, MAX_BUF);
    va_start (ap, fmt);
    vsnprintf(buf, MAX_BUF-1, fmt, ap);
    va_end (ap);

    if ( (strlen(msgBuf) + strlen(buf) + 6) > MAX_BUF )
    {
        fprintf(stderr, "%s: %s\n", LogType[LOG_ERROR], "Message buffer overflow\n");
        fprintf(stderr, "%s: %s\n", LogType[LOG_ERROR], "Requested message was: %s\n", buf);
        strcat(msgBuf, "\n"); 
        printf(msgBuf);
        return;
    }

    switch (state)
    {
        case 0: 
            strcat(msgBuf, "FAIL "); 
            if ( strlen(buf) != 0 ) strcat(msgBuf, buf); 
            strcat(msgBuf, "\n");
            fprintf(stderr, "%s: %s\n", LogType[LOG_ERROR], msgBuf);
            break;
        case 1: 
            strcat(msgBuf, "PASS ");
            if ( strlen(buf) != 0 ) strcat(msgBuf, buf);
            strcat(msgBuf, "\n");
            printf(msgBuf);
            break;
    }
}

